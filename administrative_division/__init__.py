# coding: utf-8
from django.apps import AppConfig


class AdministrativeDivisionConfig(AppConfig):
    """ Configuration de l'application """

    name = 'administrative_division'
    verbose_name = "Divisions administratives"


default_app_config = 'administrative_division.AdministrativeDivisionConfig'
