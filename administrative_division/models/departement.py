# coding: utf-8
from django.core.exceptions import ObjectDoesNotExist
from django.db import models

LISTE_REGION = (
    # Regions outre-mer
    ('01', "Guadeloupe"),
    ('02', "Martinique"),
    ('03', "Guyane"),
    ('04', "La Réunion"),
    ('06', "Mayotte"),
    # Regions métropolitaines
    ('11', "Île-de-France"),
    ('24', "Centre-Val de Loire"),
    ('27', "Bourgogne-Franche-Comté"),
    ('28', "Normandie"),
    ('32', "Hauts-de-France"),
    ('44', "Grand Est"),
    ('52', "Pays de la Loire"),
    ('53', "Bretagne"),
    ('75', "Nouvelle-Aquitaine"),
    ('76', "Occitanie"),
    ('84', "Auvergne-Rhône-Alpes"),
    ('93', "Provence-Alpes-Côte d'Azur"),
    ('94', "Corse"),
)

LISTE_DEPARTEMENT = (
    # Départements métropolitains
    ('01', 'Ain', '84'),
    ('02', 'Aisne', '32'),
    ('03', 'Allier', '84'),
    ('04', 'Alpes-de-Haute-Provence', '93'),
    ('05', 'Hautes-Alpes', '93'),
    ('06', 'Alpes-Maritimes', '93'),
    ('07', 'Ardèche', '84'),
    ('08', 'Ardennes', '44'),
    ('09', 'Ariège', '76'),
    ('10', 'Aube', '44'),
    ('11', 'Aude', '76'),
    ('12', 'Aveyron', '76'),
    ('13', 'Bouches-du-Rhône', '93'),
    ('14', 'Calvados', '28'),
    ('15', 'Cantal', '84'),
    ('16', 'Charente', '75'),
    ('17', 'Charente-Maritime', '75'),
    ('18', 'Cher', '24'),
    ('19', 'Corrèze', '75'),
    ('2A', 'Corse-du-Sud', '94'),
    ('2B', 'Haute-Corse', '94'),
    ('21', 'Côte-d\'Or', '27'),
    ('22', 'Côtes-d\'Armor', '53'),
    ('23', 'Creuse', '75'),
    ('24', 'Dordogne', '75'),
    ('25', 'Doubs', '27'),
    ('26', 'Drôme', '84'),
    ('27', 'Eure', '28'),
    ('28', 'Eure-et-Loir', '24'),
    ('29', 'Finistère', '53'),
    ('30', 'Gard', '76'),
    ('31', 'Haute-Garonne', '76'),
    ('32', 'Gers', '76'),
    ('33', 'Gironde', '75'),
    ('34', 'Hérault', '76'),
    ('35', 'Ille-et-Vilaine', '53'),
    ('36', 'Indre', '24'),
    ('37', 'Indre-et-Loire', '24'),
    ('38', 'Isère', '84'),
    ('39', 'Jura', '27'),
    ('40', 'Landes', '75'),
    ('41', 'Loir-et-Cher', '24'),
    ('42', 'Loire', '84'),
    ('43', 'Haute-Loire', '84'),
    ('44', 'Loire-Atlantique', '52'),
    ('45', 'Loiret', '24'),
    ('46', 'Lot', '76'),
    ('47', 'Lot-et-Garonne', '75'),
    ('48', 'Lozère', '76'),
    ('49', 'Maine-et-Loire', '52'),
    ('50', 'Manche', '28'),
    ('51', 'Marne', '44'),
    ('52', 'Haute-Marne', '44'),
    ('53', 'Mayenne', '52'),
    ('54', 'Meurthe-et-Moselle', '44'),
    ('55', 'Meuse', '44'),
    ('56', 'Morbihan', '53'),
    ('57', 'Moselle', '44'),
    ('58', 'Nièvre', '27'),
    ('59', 'Nord', '32'),
    ('60', 'Oise', '32'),
    ('61', 'Orne', '28'),
    ('62', 'Pas-de-Calais', '32'),
    ('63', 'Puy-de-Dôme', '84'),
    ('64', 'Pyrénées-Atlantiques', '75'),
    ('65', 'Hautes-Pyrénées', '76'),
    ('66', 'Pyrénées-Orientales', '76'),
    ('67', 'Bas-Rhin', '44'),
    ('68', 'Haut-Rhin', '44'),
    ('69', 'Rhône', '84'),
    ('70', 'Haute-Saône', '27'),
    ('71', 'Saône-et-Loire', '27'),
    ('72', 'Sarthe', '52'),
    ('73', 'Savoie', '84'),
    ('74', 'Haute-Savoie', '84'),
    ('75', 'Paris', '11'),
    ('76', 'Seine-Maritime', '28'),
    ('77', 'Seine-et-Marne', '11'),
    ('78', 'Yvelines', '11'),
    ('79', 'Deux-Sèvres', '75'),
    ('80', 'Somme', '32'),
    ('81', 'Tarn', '76'),
    ('82', 'Tarn-et-Garonne', '76'),
    ('83', 'Var', '93'),
    ('84', 'Vaucluse', '93'),
    ('85', 'Vendée', '52'),
    ('86', 'Vienne', '75'),
    ('87', 'Haute-Vienne', '75'),
    ('88', 'Vosges', '44'),
    ('89', 'Yonne', '27'),
    ('90', 'Territoire de Belfort', '27'),
    ('91', 'Essonne', '11'),
    ('92', 'Hauts-de-Seine', '11'),
    ('93', 'Seine-Saint-Denis', '11'),
    ('94', 'Val-de-Marne', '11'),
    ('95', 'Val-d\'Oise', '11'),
    # Départements et territoires d'outre-mer
    ('971', 'Guadeloupe', '01'),
    ('972', 'Martinique', '02'),
    ('973', 'Guyane', '03'),
    ('974', 'La Réunion', '04'),
    ('975', 'Saint-Pierre-et-Miquelon', None),
    ('976', 'Mayotte', '05'),
    ('977', 'Saint-Barthélemy', None),
    ('978', 'Saint-Martin', None),
    ('984', 'Terres australes et antarctiques françaises', None),
    ('986', 'Wallis et Futuna', None),
    ('987', 'Polynésie française', None),
    ('988', 'Nouvelle-Calédonie', None),
    ('989', 'Île de Clipperton', None),
)

CHOIX_DEPARTEMENT = tuple([(dep[0], '%s - %s' % (dep[0], dep[1]),) for dep in LISTE_DEPARTEMENT])


class DepartementManager(models.Manager):
    """ Manager """

    def get_by_natural_key(self, name):
        return self.get(name=name)

    def configured(self):
        """ Renvoyer les départements ayant une instance configurée """
        return self.filter(instance__isnull=False)

    def unconfigured(self):
        """ Renvoyer les départements non abonnés """
        return self.filter(instance__isnull=True)

    def by_cities(self, cities, as_initial=False):
        """
        Renvoyer les départements qui correspondent aux villes passées

        :param cities: liste ou queryset de villes
        :param as_initial: renvoyer une liste des ID au lieu du queryset (pour Form.initial)
        :type as_initial: bool
        :returns: un queryset des départements, ou une liste des ID des départements
        """
        result = self.none()
        if isinstance(cities, (models.Manager, models.QuerySet)):
            cities = cities.all()
            result = self.filter(arrondissement__commune__id__in=[c.id for c in cities])
        if as_initial:
            return list(result.values_list('id', flat=True))
        else:
            return result

    def get_by_name(self, name):
        """
        Renvoyer un département portant le nom passé en argument

        :return: une instance de département, ou None si non trouvé
        """
        try:
            return self.get(name__iexact=name)
        except Departement.DoesNotExist:
            return None


class Departement(models.Model):
    """ Division de niveau 2 (département) : total de 96+ en France métropolitaine """

    # Champs
    name = models.CharField("Département", unique=True, max_length=3, choices=CHOIX_DEPARTEMENT)
    objects = DepartementManager()

    # Getter
    def get_instance(self):
        try:
            return self.instance
        except ObjectDoesNotExist:
            from core.models import Instance
            return Instance.objects.get_master()

    def get_name(self):
        """ Afficher le nom lisible du département """
        try:
            return self.get_name_display()
        except KeyError:
            return self.name

    # Overrides
    def __str__(self):
        return self.get_name_display()

    def natural_key(self):
        return self.name,

    # Méta
    class Meta:
        verbose_name = "Département"
        verbose_name_plural = "Départements"
        ordering = ['name']
        app_label = 'administrative_division'
        default_related_name = 'departements'
