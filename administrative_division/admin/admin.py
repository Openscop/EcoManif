# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from ..forms import CommuneForm, DepartementForm
from ..models import Commune, Arrondissement, Departement


class ArrondissementAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'code', 'name', 'get_departement']
    list_filter = ['departement__name']
    search_fields = ['name', 'departement__name']


@admin.register(Commune)
class CommuneAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """

    # Configuration
    list_display = ['pk', 'name', 'zip_code', 'arrondissement', 'get_departement']
    list_filter = ['arrondissement__departement']
    search_fields = ['name', 'zip_code', 'ascii_name']
    form = CommuneForm


@admin.register(Arrondissement)
class ArrondissementAdmin(ImportExportActionModelAdmin):
    """ Configuration admin des départements """

    # Configuration
    list_display = ['pk', 'code', 'name', 'get_departement']
    list_filter = ['departement__name']
    search_fields = ['name', 'departement__name']


@admin.register(Departement)
class DepartementAdmin(ImportExportActionModelAdmin):
    """ Configuration admin des départements """

    # Supprimer tous les boutons "ajouter un departement" dont le bouton vert "plus" à coté du champ departement des pages d'aide
    def has_add_permission(self, request):
        return False

    # Configuration
    list_display = ['pk', 'name']
    search_fields = ['name']
    form = DepartementForm
