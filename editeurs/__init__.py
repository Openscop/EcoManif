from django.apps import AppConfig


class EditeursConfig(AppConfig):
    """ Configuration d'Application """

    # Configuration
    name = 'editeurs'
    verbose_name = "Editeurs"

default_app_config = 'editeurs.EditeursConfig'
