from django.contrib import admin
from related_admin import RelatedFieldAdmin

from .models import Editeur


@admin.register(Editeur)
class EditeurAdmin(RelatedFieldAdmin):
    """ Configuration de l'admin Editeur """
    list_display = ['pk', 'user']
