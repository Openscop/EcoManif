# Generated by Django 2.1.2 on 2019-02-12 13:41

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Editeur',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, related_name='editeur', to='core.User', verbose_name='utilisateur')),
            ],
            options={
                'verbose_name': 'éditeur',
                'verbose_name_plural': 'éditeurs',
                'default_related_name': 'editeurs',
            },
        ),
    ]
