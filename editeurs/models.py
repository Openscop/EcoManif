from django.db import models
from django.conf import settings
from django.urls import reverse

# Create your models here.
class Editeur(models.Model):
    """ Utilisateur qui écrivent les pages statiques """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="editeur", verbose_name="utilisateur", on_delete=models.CASCADE)

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.user.get_username()

    def get_url(self):
        """ Renvoyer l'URL de l'objet """
        return reverse('admin:editeurs_editeur_change', args=(self.pk,))

    class Meta:
        verbose_name = "éditeur"
        verbose_name_plural = "éditeurs"
        app_label = "editeurs"
        default_related_name = "editeurs"
