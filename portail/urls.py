# coding: utf-8
from django.urls import path, re_path

from .views.calendar import Calendar
from .views.calendar import FilteredCalendar
from .views.index import HomePage
from .views.stat import Stat


app_name = 'portail'
urlpatterns = [

    # Accueil
    path('', HomePage.as_view(), name='home_page'),

    # Calendrier
    re_path('calendar/(?P<activite>\d+)/', FilteredCalendar.as_view(), name="filtered_calendar"),
    path('calendar/', Calendar.as_view(), name="calendar"),
    path('stat/', Stat.as_view(), name="stat"),

]
