/**
 * Created by knt on 17/03/17.
 */

// jQuery plugin to prevent double submission of forms
jQuery.fn.preventDoubleSubmission = function() {
  $(this).on('submit',function(e){
    var $form = $(this);
    if ($form.data('submitted') === true) {
      $form.find("input#submit-id-submit").attr("value","Action en cours. Veuillez patienter...");
      $form.find("button[type=\"submit\"]").text("Action en cours. Veuillez patienter...");
      // Previously submitted - don't submit again
      e.preventDefault();
      return false;
    } else {
      // Mark it so that the next submit can be ignored
      $form.find("input#submit-id-submit").after("<i class=\"far fa-spinner fa-2x fa-spin\" style=\"position: absolute; top: -10px; margin-left: 9px; color: #015A70; \"></i>");
      $form.find("button[type=\"submit\"]").after("<i class=\"far fa-spinner fa-2x fa-spin\" style=\"position: relative; top: 6px; margin-left: 9px; color: #015A70; \"></i>");
      $form.data('submitted', true);
    }
  });

  // Keep chainability
  return this;
};
