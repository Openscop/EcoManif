# coding: utf-8
import factory

from evenements.factories import AvtmFactory
from core.factories import UserFactory
from notifications.models import Action, Notification


class ActionFactory(factory.django.DjangoModelFactory):
    """ Factory pour les actions """

    user = factory.SubFactory(UserFactory)
    manif = factory.SubFactory(AvtmFactory)

    class Meta:
        model = Action


class NotificationFactory(factory.django.DjangoModelFactory):
    """ Factory pour les notifications """

    user = factory.SubFactory(UserFactory)
    manif = factory.SubFactory(AvtmFactory)

    class Meta:
        model = Notification
