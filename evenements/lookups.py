# coding: utf-8
from ajax_select import register, LookupChannel
from unidecode import unidecode

from evenements.models.manifestation import Manif


@register('manifestation')
class ManifLookup(LookupChannel):
    """ Lookup AJAX des manifestations """

    # Configuration
    model = Manif

    # Overrides
    def get_query(self, q, request):
        q = unidecode(q)
        return self.model.objects.filter(instance__isnull=False, nom__icontains=q).order_by('nom')[:20]

    def format_item_display(self, item):
        return u"<span class='tag'>{nom}</span>".format(nom=item.nom)

    def format_match(self, item):
        return u"<span class='tag'>{nom}</span>".format(nom=item.nom)
