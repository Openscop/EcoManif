# coding: utf-8
from django.urls import path, re_path

from .views.dashboard import TableauDeBordOrganisateur
from .views.charte import CharteCreate, CharteUpdate, CharteDiplome, CharteImpression, CharteLabel
from .views.manif import (PreambuleCreationEvenementView, ManifPublicDetail, ManifCreate, ManifDetail, ManifSend,
                          ManifUpdate, ManifFilesUpdate)
from .views.documentcomplementaire import DocumentComplementaireRequestView, DocumentComplementaireProvideView
from .views.sansinstance import SansInstanceManifCreate
from organisateurs.views.structure import StructureCreate


app_name = 'evenements'
urlpatterns = [

    # Tableau de bord principal
    path('tableau-de-bord-organisateur/', TableauDeBordOrganisateur.as_view(), name="tableau-de-bord-organisateur"),
    path('structure/add', StructureCreate.as_view(), name='structure_add'),

    # Manifestation sans type (non supporté)
    path('sansinstance/add/', SansInstanceManifCreate.as_view(), name="manif_sansinstance_add"),

    # Manifestation
    path('evenement/preambule/', PreambuleCreationEvenementView.as_view(), name='preambule_creation_evenement'),
    path('evenement/creation/', ManifCreate.as_view(), name='manif_add'),
    re_path('evenement/(?P<pk>\d+)/edition/', ManifUpdate.as_view(), name="manif_update"),
    re_path('evenement/(?P<pk>\d+)/edition_fichiers/', ManifFilesUpdate.as_view(), name="manif_files_update"),
    re_path('evenement/(?P<pk>\d+)/public/', ManifPublicDetail.as_view(), name="manif_public"),
    re_path('evenement/(?P<pk>\d+)/envoi/', ManifSend.as_view(), name="manif_send"),
    re_path('evenement/(?P<manif_pk>\d+)/charte/creation/', CharteCreate.as_view(), name="charte_add"),
    re_path('evenement/(?P<manif_pk>\d+)/charte/edition/', CharteUpdate.as_view(), name="charte_update"),
    re_path('evenement/(?P<manif_pk>\d+)/charte/diplome/', CharteDiplome.as_view(), name="charte_dipl"),
    re_path('evenement/(?P<manif_pk>\d+)/charte/impression/', CharteImpression.as_view(), name="charte_impr"),
    re_path('evenement/(?P<manif_pk>\d+)/charte/label/(?P<label>\w+)', CharteLabel.as_view(), name="charte_label"),
    re_path('evenement/(?P<manif_pk>\d+)/doc_complementaire/add', DocumentComplementaireRequestView.as_view(), name="demander_doc_complementaire"),
    re_path('evenement/(?P<pk>\d+)/', ManifDetail.as_view(), name="manif_detail"),

    re_path('doc_complementaire/(?P<pk>\d+)/', DocumentComplementaireProvideView.as_view(), name="fournir_doc_complementaire"),

]
