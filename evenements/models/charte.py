from django.db import models

from . import Manif


class Charte(models.Model):
    """
    Charte d'engagement 'Evénement Sport Nature & développement durable'
    """
    CHARTE_GROUP = (
        "Conservation et gestion durable de la biodiversité et des ressources naturelles",
        "Consommation et production responsables",
        "Sensibilisation et éco-communication",
        "Transport et mobilité durable & energie",
        "Responsabilité sociale du sport et solidarité"
    )
    CHARTE_GROUP_MEMBER = (
        ("reglementation", "traces", "amenagements", "nettoyage", "gestionnaire", "contact", "protection", "balisage", "respect_env", "autre_01", "autre_02"),
        ("dechets", "benevoles", "site_propre", "vaisselle", "eau_reseau", "conditionnement", "quantite", "ingredients", "vegetarien", "producteurs", "materiel", "location", "toilettes", "autre_11", "autre_12"),
        ("reseaux", "papiers", "impression_papier", "impression_encre", "support_com", "gadjet", "responsable", "pratiques", "dispositifs", "site_internet", "charte_eco", "sponsors", "pack_eco", "caritatif", "autre_21", "autre_22"),
        ("transport_commun", "covoiturage", "parking", "transport_alter", "recompense", "pieton", "acces_facile", "gaspillage", "planif_elec", "groupe_elec", "conso_elec", "hebergeur", "autre_31", "autre_32"),
        ("remerciement", "respect_pop", "riverain", "mobilite", "handicap", "tarif", "partenariat", "sante", "secourisme", "autre_41", "autre_42"),
    )
    # Dictionnaire précisant l'obligation, le poids et le libellé de chaque question
    #     { "texte" : ( requis, poids, libellé ) }
    CHARTE_MEMBER = {
        "reglementation": (1, 1, "Je prends en compte les informations concernant la réglementation des espaces naturels traversés par mon évènement"),
        "traces": (1, 1, "J'adapte les tracés des parcours en fonction des enjeux naturalistes des sites et privilégie les sentiers existants"),
        "amenagements": (1, 1, "Je réalise les aménagements du site en dehors des espaces naturels sensibles"),
        "nettoyage": (1, 1, "J'organise le nettoyage du site dès la fin de l'épreuve : ramassage des déchets, rubalise, affichage..."),
        "gestionnaire": (0, 1, "Je prends contact avec le gestionnaire des espaces naturels traversés (s'il y en a un)"),
        "contact": (0, 1, "Je prends contact avec les autres usagers pour les informer et coordonner les différentes activités"),
        "protection": (0, 1, "J'informe les participants et spectateurs des enjeux environnementaux du site et des consignes visant à protéger les lieux"),
        "balisage": (0, 1, "J'optimise au maximum l'utilisation du balisage et privilégie les matériaux réutilisables et/ou écologiques"),
        "respect_env": (0, 1, "J'inscris au réglement de l'épreuve un article spécifique au respect de l'environnement"),
        "autre_01": (0, 1),
        "autre_02": (0, 1),
        "dechets": (1, 1, "Je mets à disposition des participants et des bénévoles un système de tri bi-flux efficace"),
        "benevoles": (1, 1, "Je mets en place une équipe de bénévoles en charge de corriger les erreurs de tri et d’évacuer les sacs dans les bennes dédiées"),
        "site_propre": (1, 1, "Je m’assure qu’il n’y ait plus de déchets sur le site après la manifestation"),
        "vaisselle": (0, 1, "J’utilise autant que possible de la vaisselle et des gobelets lavables (non jetables) pour les ravitaillements, les repas et les buvettes"),
        "eau_reseau": (0, 1, "J’utilise l’eau du réseau local, pour éviter de recourir aux bouteilles plastiques"),
        "conditionnement": (1, 1, "J’achète des produits en vrac ou en gros conditionnement pour éviter le suremballage"),
        "quantite": (1, 1, "J’optimise la gestion des quantités commandées pour limiter le gaspillage"),
        "ingredients": (0, 1, "J’utilise plus de 3 ingrédients locaux pour la restauration"),
        "vegetarien": (0, 1, "Je propose un menu alternatif faiblement carné ou végétarien"),
        "producteurs": (0, 1, "Je valorise les producteurs locaux pendant mon évènement"),
        "materiel": (0, 1, "Je privilégie le matériel recyclé, d’occasion, la location, ou le matériel de bonne qualité pour éviter le renouvellement fréquent"),
        "location": (0, 1, "Je privilégie la location ou la mutualisation pour tout le matériel événementiel"),
        "toilettes": (0, 1, "J’utilise des toilettes sèches"),
        "autre_11": (0, 1),
        "autre_12": (0, 1),
        "reseaux": (1, 1, "J’utilise les réseaux sociaux et la messagerie électronique pour diffuser le maximum d’informations aux acteurs concernés"),
        "papiers": (1, 1, "J’optimise la diffusion des documents papiers"),
        "impression_papier": (0, 1, "J’imprime mes documents sur du papier recyclé ou labellisé FSC/PEFC"),
        "impression_encre": (0, 1, "J’imprime mes documents avec des encres végétales ou chez un imprimeur \"imprim’vert\""),
        "support_com": (0, 1, "J’utilise essentiellement des supports de communication dématérialisés ou réutilisables"),
        "gadjet": (0, 1, "Je n’offre pas de gadjets, goodies non éco-responsables"),
        "responsable": (1, 1, "J’identifie au sein du comité d’organisation un responsable de la démarche \"Développement Durable\""),
        "pratiques": (1, 1, "Je sensibilise les bénévoles et collaborateurs aux pratiques éco-responsables"),
        "dispositifs": (1, 1, "J’informe et je sensibilise le public sur les dispositifs mis en place"),
        "site_internet": (0, 1, "Mon site internet comporte une rubrique \"développement durable\""),
        "charte_eco": (0, 1, "Je rédige et diffuse une \"charte de l’éco-participant\""),
        "sponsors": (0, 1, "J’implique les sponsors et les partenaires dans ma démarche éco-responsable"),
        "pack_eco": (0, 1, "Je valorise ma démarche en affichant les outils de sensibilisation du pack éco-événement"),
        "caritatif": (0, 1, "J’implique mon événement dans une action caritative"),
        "autre_21": (0, 1),
        "autre_22": (0, 1),
        "transport_commun": (1, 1, "J’informe les participants et le public sur les dispositifs de transport en commun, via mon site internet"),
        "covoiturage": (0, 1, "J’inscris mon événement sur un site de covoiturage"),
        "parking": (0, 1, "Je prévois un parking à vélo et des emplacements privilégiés pour les co-voiturages"),
        "transport_alter": (0, 1, "Je prévois un système de transports alternatifs pour l’organisation et les participants"),
        "recompense": (0, 1, "Je récompense les participants qui utilisent des modes de transports doux"),
        "pieton": (0, 1, "Je rends au maximum la manifestation piétonne"),
        "acces_facile": (0, 1, "Le lieu choisit présente des facilités d’accès en transport en commun"),
        "gaspillage": (1, 1, "Je veille à ne pas gaspiller l’énergie"),
        "planif_elec": (0, 1, "Je  planifie les périodes de mise sous tension des éclairages et des équipements électriques"),
        "groupe_elec": (0, 1, "J’évite l’usage de groupe électrogène"),
        "conso_elec": (0, 1, "Je privilégie les équipements électriques faiblement consommateur d’énergie"),
        "hebergeur": (0, 1, "J’implique les hébergeurs locaux dans la démarche éco-responsable de mon évènement"),
        "autre_31": (0, 1),
        "autre_32": (0, 1),
        "remerciement": (1, 1, "Je valorise l’engagement des bénévoles par un repas, des remerciements, une photo souvenir..."),
        "respect_pop": (1, 1, "J’incite les participants et le public à respecter les populations locales, les propriétaires et les autres usagers du site"),
        "riverain": (0, 1, "J’informe les riverains de la présence de l’événement"),
        "mobilite": (0, 1, "Je mets en place toutes les facilités pour les personnes à mobilités réduites"),
        "handicap": (0, 1, "J’intègre des personnes en situation de handicap dans l’équipe d’organisation et de bénévoles"),
        "tarif": (0, 1, "Je mets en place une politique tarifaire permettant de rendre mon évènement accessible à tous"),
        "partenariat": (0, 1, "J’établi un partenariat avec une association solidaire"),
        "sante": (0, 1, "Je mets en place une information et/ou prévention de la santé par et pendant l’activité physique"),
        "secourisme": (0, 1, "Je forme mes dirigeant et mes bénévoles aux gestes qui sauvent (défibrillateur, autre...)"),
        "autre_41": (0, 1),
        "autre_42": (0, 1),
    }

    # Texte d'aide
    AIDE_REGLEMENTATION = "consulter votre carte communale sur le site <a target='_blank' href='http://www.rdbrmc-travaux.com/basedreal/Accueil.php'>CARMEN / DREAL</a>"
    AIDE_AMENAGEMENT = "(départ, arrivée, stand, parking...)"
    AIDE_CONTACT = "(sportives, chasse, exploitation forestière et agricole...)"
    AIDE_BALISAGE = "(plâtre, chaux...)"
    AIDE_RESPECT_ENV = "(disqualification, pénalisation)"
    AIDE_DECHETS = "(nombre et emplacements des points tri, affichage spécifique...) Voir avec la collectivité en charge des déchets"
    AIDE_PRODUCTEURS = "(stand, marché des producteur...)"
    AIDE_LOCATION = "(tente, décoration...)"
    AIDE_PAPIER = "(nombre, format recto-verso, sans aplat de couleur)"
    AIDE_TRANSPORT_ALTER = "(navettes, vélo pour les bénévoles...)"
    AIDE_CONSO_ELEC = "(référence aux étiquettes énergétiques)"
    AIDE_PARTENARIAT = "(soutien financier, collecte, campagne de sensibilisation...)"

    # Conservation et gestion durable de la biodiversité et des ressources naturelles
    reglementation = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['reglementation'][2], help_text=AIDE_REGLEMENTATION)
    gestionnaire = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['gestionnaire'][2])
    traces = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['traces'][2])
    amenagements = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['amenagements'][2], help_text=AIDE_AMENAGEMENT)
    nettoyage = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['nettoyage'][2])
    contact = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['contact'][2], help_text=AIDE_CONTACT)
    protection = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['protection'][2])
    balisage = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['balisage'][2], help_text=AIDE_BALISAGE)
    respect_env = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['respect_env'][2], help_text=AIDE_RESPECT_ENV)
    autre_01 = models.TextField(verbose_name="Action supplémentaire #1", blank=True, null=True)
    autre_02 = models.TextField(verbose_name="Action supplémentaire #2", blank=True, null=True)

    # Consommation et production responsables
    dechets = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['dechets'][2], help_text=AIDE_DECHETS)
    benevoles = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['benevoles'][2])
    site_propre = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['site_propre'][2])
    vaisselle = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['vaisselle'][2])
    eau_reseau = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['eau_reseau'][2])
    conditionnement = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['conditionnement'][2])
    quantite = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['quantite'][2])
    ingredients = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['ingredients'][2])
    vegetarien = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['vegetarien'][2])
    producteurs = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['producteurs'][2], help_text=AIDE_PRODUCTEURS)
    materiel = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['materiel'][2])
    location = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['location'][2], help_text=AIDE_LOCATION)
    toilettes = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['toilettes'][2])
    autre_11 = models.TextField(verbose_name="Action supplémentaire #1", blank=True, null=True)
    autre_12 = models.TextField(verbose_name="Action supplémentaire #2", blank=True, null=True)

    # Sensibilisation et éco-communication
    reseaux = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['reseaux'][2])
    papiers = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['papiers'][2], help_text=AIDE_PAPIER)
    impression_papier = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['impression_papier'][2])
    impression_encre = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['impression_encre'][2])
    support_com = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['support_com'][2])
    gadjet = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['gadjet'][2])
    responsable = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['responsable'][2])
    site_internet = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['site_internet'][2])
    pratiques = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['pratiques'][2])
    dispositifs = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['dispositifs'][2])
    charte_eco = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['charte_eco'][2])
    sponsors = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['sponsors'][2])
    pack_eco = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['pack_eco'][2])
    caritatif = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['caritatif'][2])
    autre_21 = models.TextField(verbose_name="Action supplémentaire #1", blank=True, null=True)
    autre_22 = models.TextField(verbose_name="Action supplémentaire #2", blank=True, null=True)

    # Transport et mobilité durable & energie
    transport_commun = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['transport_commun'][2])
    covoiturage = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['covoiturage'][2])
    parking = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['parking'][2])
    transport_alter = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['transport_alter'][2], help_text=AIDE_TRANSPORT_ALTER)
    recompense = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['recompense'][2])
    pieton = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['pieton'][2])
    acces_facile = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['acces_facile'][2])
    gaspillage = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['gaspillage'][2])
    planif_elec = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['planif_elec'][2])
    groupe_elec = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['groupe_elec'][2])
    conso_elec = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['conso_elec'][2], help_text=AIDE_CONSO_ELEC)
    hebergeur = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['hebergeur'][2])
    autre_31 = models.TextField(verbose_name="Action supplémentaire #1", blank=True, null=True)
    autre_32 = models.TextField(verbose_name="Action supplémentaire #2", blank=True, null=True)

    # Responsabilité sociale du sport et solidarité
    remerciement = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['remerciement'][2])
    respect_pop = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['respect_pop'][2])
    riverain = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['riverain'][2])
    mobilite = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['mobilite'][2])
    handicap = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['handicap'][2])
    tarif = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['tarif'][2])
    partenariat = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['partenariat'][2], help_text=AIDE_PARTENARIAT)
    sante = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['sante'][2])
    secourisme = models.BooleanField(default=False, verbose_name=CHARTE_MEMBER['secourisme'][2])
    autre_41 = models.TextField(verbose_name="Action supplémentaire #1", blank=True, null=True)
    autre_42 = models.TextField(verbose_name="Action supplémentaire #2", blank=True, null=True)

    # Manifattachée
    manif = models.OneToOneField(Manif, on_delete=models.CASCADE)

    def __str__(self):
        return "Charte de " + self.manif.nom

    def score_par_group(self):
        score = []
        total = []
        for list in self.CHARTE_GROUP_MEMBER:
            resu, max = 0, 0
            for item in list:
                # Vérifier si la question DOIT être cochée
                if self.CHARTE_MEMBER[item][0] and self.__getattribute__(item) != self.CHARTE_MEMBER[item][0]:
                    resu, max = 0, 1
                    break
                # ne pas compter les champs texte non rempli
                if self.__getattribute__(item) != None and self.__getattribute__(item) != "":
                    # Attribuer le poids de la question si cochée
                    if self.__getattribute__(item):
                        resu += self.CHARTE_MEMBER[item][1]
                    max += self.CHARTE_MEMBER[item][1]
            percent = resu * 100 / max
            score.append(int(percent))
            total.append(resu)
        return score, total

    def score_total(self):
        score, resu = self.score_par_group()
        return int(sum(score)/len(score)), sum(resu)

    def niveau_grp(self):
        exam = []
        inst = self.manif.get_instance()
        scoring = [
            [inst.group1_argent, inst.group1_or],
            [inst.group2_argent, inst.group2_or],
            [inst.group3_argent, inst.group3_or],
            [inst.group4_argent, inst.group4_or],
            [inst.group5_argent, inst.group5_or],
        ]
        score, resu = self.score_par_group()
        for indice, total in enumerate(resu):
            # Vérifier le score mini de chaque groupe (si 1 obligatoire manque, total = 0)
            # si total != 0, c'est bronze minimum puisque tous les obligatoires sont ok
            if total == 0:
                stat = 'nok'
            elif total < scoring[indice][0]:
                stat = 'okb'
            elif total < scoring[indice][1]:
                stat = 'oka'
            else:
                stat = 'oko'
            exam.append(stat)
        return exam

    def diplome(self):
        percent, total = self.score_total()
        exam = self.niveau_grp()
        inst = self.manif.get_instance()
        # Attribuer un diplôme suivant le score total et les scores minis
        if 'nok' in exam:
            return "echec"
        if total < inst.global_argent:
            return "bronze"
        elif total < inst.global_or:
            return "argent"
        else:
            return "or"
