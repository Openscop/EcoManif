from django.db import models
from django.contrib.contenttypes.models import ContentType
from model_utils.managers import InheritanceQuerySet
from django.urls import reverse
import datetime

from configuration.directory import UploadPath
from sports.models.federation import Federation
from core.models.instance import Instance
from notifications.models.action import Action
from notifications.models.notification import Notification
from referents.models import Referent


class ManifQuerySet(InheritanceQuerySet):
    """ Queryset des manifestations """

    # Getter
    def par_instance(self, request=None, instances=None):
        """
        Renvoyer les manifestations pour l'utilisateur ou les instances

        :param request: requête HTTP
        :param instances: liste d'instances
        :type instances: list<core.instance>
        """
        if request and not request.user.is_anonymous:
            # Renvoyer les manifestations pour l'utilisateur s'il est renseigné
            return self.filter(instance=request.user.get_instance())
        elif instances:
            # Renvoyer les manifestations pour les instances si elles sont renseignées
            return self.filter(instance__in=instances)
        else:
            return self

    def par_organisateur(self, organisateur):
        """ Renvoyer les manifestations créées par l'organisateur """
        return self.filter(structure__organisateur=organisateur)

    def prisesencharge(self):
        """ Renvoyer les manifestations prises en charge """
        return self.filter(instance__departement__isnull=False)

    def nonprisesencharge(self):
        """ Renvoyer les manifestations non prises en charge """
        return self.filter(instance__departement__isnull=True)


class Manif(models.Model):
    """ Demande de manifestation sportive """

    CHAMPS_REQUIS = ['nom', 'date_debut', 'date_fin', 'activite', 'ville_depart', 'nb_participants',
                     'nom_contact', 'prenom_contact', 'tel_contact']

    # Texte d'aide et descriptions
    AIDE_DESCRIPTION = "précisez les modalités d'organisation et les caractéristiques de la manifestation"
    DESC_DEPARTEMENTS = "autres départements traversés par la manifestation"
    AIDE_MULTISELECT = "maintenez appuyé « Ctrl », ou « Commande (touche pomme) » sur un Mac, pour en sélectionner plusieurs."
    AIDE_NB_PARTICIPANTS = "renseigner le nombre de participants de la précedente édition ou le nombre de participants estimé si première édition"
    AIDE_CONVENTION = "convention de partenariat signée avec un parc naturel traversé"
    # Chemins d'upload
    UP_CARTO = UploadPath('cartographie')
    UP_CONV = UploadPath('convention')
    UP_EXTRA = UploadPath('doc_complementaires')

    # Attributs généraux
    instance = models.ForeignKey('core.instance', null=True, verbose_name="Configuration d'instance", on_delete=models.SET_NULL)
    structure = models.ForeignKey('organisateurs.structure', verbose_name="structure", on_delete=models.CASCADE)
    nom = models.CharField(verbose_name="nom de la manifestation", max_length=255, null=True, blank=True, unique=True)
    date_creation = models.DateField("date de création", auto_now_add=True)
    date_debut = models.DateTimeField("date de début", null=True, blank=True)
    date_fin = models.DateTimeField("date de fin", null=True, blank=True)
    description = models.TextField("description", null=True, blank=True, help_text=AIDE_DESCRIPTION)
    activite = models.ForeignKey('sports.activite', verbose_name="activité", on_delete=models.CASCADE)
    ville_depart = models.ForeignKey('administrative_division.commune', verbose_name="commune de départ", on_delete=models.CASCADE)
    nb_participants = models.PositiveIntegerField("nombre de participants", null=True, blank=True, help_text=AIDE_NB_PARTICIPANTS)
    nb_spectateurs = models.PositiveIntegerField("nombre de spectateurs attendus", null=True, blank=True)
    parcs = models.ManyToManyField('parcs_naturels.parcnaturel', related_name="manifestations", verbose_name="parcs traversés", blank=True)
    villes_traversees = models.ManyToManyField('administrative_division.commune', related_name="%(app_label)s_%(class)s_crossed_by", verbose_name="communes traversées", blank=True)
    departements_traverses = models.ManyToManyField('administrative_division.departement', verbose_name=DESC_DEPARTEMENTS, blank=True)
    prenom_contact = models.CharField("prénom", max_length=200, null=True, blank=True)
    nom_contact = models.CharField("nom de famille", max_length=200, null=True, blank=True)
    tel_contact = models.CharField("numéro de téléphone", max_length=14, null=True, blank=True)
    email_contact = models.EmailField("adresse email", max_length=200, blank=True, default='')

    # Informations de publication
    finalise = models.BooleanField(default=False, verbose_name="manifestation envoyée")
    prive = models.BooleanField(default=False, verbose_name="Ne pas publier la manifestation dans le calendrier")  # Organisateurs
    cache = models.BooleanField(default=False, verbose_name="Ne pas publier la manifestation dans le calendrier")  # Instructeurs
    afficher_adresse_structure = models.BooleanField(default=True, verbose_name="Afficher les coordonnées de la structure dans le calendrier")  # Organisateur

    # Fichiers joints
    cartographie = models.FileField(upload_to=UP_CARTO, blank=True, null=True, verbose_name="cartographie de parcours", max_length=512)
    convention = models.FileField(upload_to=UP_CONV, blank=True, null=True, verbose_name="convention de partenariat", help_text=AIDE_CONVENTION, max_length=512)
    docs_additionels = models.FileField(upload_to=UP_EXTRA, blank=True, null=True, verbose_name="documents complémentaires", max_length=512)

    objects = ManifQuerySet.as_manager()

    # Overrides
    def __str__(self):
        if self.nom:
            return self.nom.title()
        return ''

    def save(self, *args, **kwargs):
        # Définir l'instance à celle de la ville de départ
        if self.ville_depart and not self.instance and not self.ville_depart.get_instance().is_master():
            self.instance = self.ville_depart.get_departement().get_instance()
        super().save(*args, **kwargs)

    # Getter
    def get_absolute_url(self):
        """ Renvoyer l'URL de consultation """
        url = ''.join(['evenements:', ContentType.objects.get_for_model(self).model, '_detail'])
        return reverse(url, kwargs={'pk': self.pk})

    def get_referent_url(self):
        """ Renvoyer l'URL de consultation pour un referent """
        url = ''.join(['referents:', ContentType.objects.get_for_model(self).model, '_detail-referent'])
        return reverse(url, kwargs={'pk': self.pk})

    def get_instance(self):
        return self.instance

    def get_departement_depart_nom(self):
        """ Renvoyer le nom simple du département de départ """
        return self.ville_depart.get_departement().name

    def get_federation(self):
        """ Renvoyer la fédération pour le département et pour la discipline """
        return Federation.objects.get_for_departement(self.activite.discipline, self.ville_depart.get_departement())

    def get_organisateur(self):
        """ Renvoyer l'organisateur """
        return self.structure.organisateur

    def get_departements_traverses(self):
        """ Renvoyer les départements traversés """
        return list(set(list(self.departements_traverses.all()) + [self.ville_depart.get_departement()]))

    def get_villes_traversees(self):
        """ Renvoyer les communes traversées """
        return list(set(list(self.villes_traversees.all()) + [self.ville_depart]))

    def get_nom_departements_traverses(self):
        """ Renvoyer les noms (01, 42, 78 etc.) des départements traversés """
        return [departement.name for departement in self.get_departements_traverses()]

    def pas_supportee(self):
        """ Renvoyer si la manifestation n'est pas prise en charge """
        return self.instance is None or self.instance.departement is None

    def visible_dans_calendrier(self):
        """ Renvoyer l'état de visibilité dans le calendrier """
        return not (self.prive or self.cache)
    visible_dans_calendrier.short_description = "Dans le calendrier"
    visible_dans_calendrier.boolean = True

    def get_federation_defaut(self):
        """ Renvoyer la fédération disponible qui correspond le mieux à cette manif """
        return Federation.objects.get_for_departement(self.activite.discipline, self.ville_depart.get_departement())

    def get_delai_legal(self):
        """ Renvoie le délai légal en jours pour déclarer ou envoyer la demande d'autorisation """
        return self.instance.get_manifestation_delay()

    def get_date_limite(self):
        """ Retourne la date limite pour déclarer ou envoyer la demande d'autorisation """
        return (self.date_debut - datetime.timedelta(days=self.get_delai_legal())).replace(hour=23, minute=59)

    def cinq_jours_restants(self):
        """ Renvoie true or false suivant le nombre de semaines restantes (sous 2 semaines ou plus de 2 semaines) """
        return self.get_date_limite().date() < (datetime.date.today() + datetime.timedelta(days=5))

    def delai_depasse(self):
        """ Si le délai officiel est dépassé, renvoie True, sinon, renvoie False """
        return datetime.date.today() > self.get_date_limite().date()

    def get_manifs_meme_jour(self, include_self=False):
        """
        Renvoyer les manifestations qui ont lieu aux mêmes dates que cette manifestation

        (et qui bien entendu, ne sont pas cette même manifestation)

        Dans la pratique, si cette manifestation est nommée e1, et la manifestation à
        comparer est nommée e2, les deux manifestations coïncident si :
        - (e2.debut <= e1.debut ET e2.fin >= e1.debut) OU
        - (e2.debut <= e1.fin ET e2.fin >= e1.fin) OU
        - (e2.debut >= e1.debut ET e2.fin <= e1.fin)

        Et plus simplement, elles coïncident si :
        - NON (e2.debut > e1.fin OU e2.fin < e1.debut)
        """
        exclusion = {} if include_self else {'nom': self.nom}
        return Manif.objects.exclude(models.Q(date_debut__gt=self.date_fin) | models.Q(date_fin__lt=self.date_debut)).exclude(**exclusion).distinct()

    def get_manifs_meme_jour_meme_ville(self, include_self=False):
        """
        Renvoyer les manifestations qui passent dans les mêmes villes

        que la manifestation actuelle.
        """
        communes = self.get_villes_traversees()
        return self.get_manifs_meme_jour(include_self=include_self).filter(models.Q(ville_depart__in=communes) | models.Q(villes_traversees__in=communes)).distinct()

    def get_breadcrumbs(self):
        """ Renvoie les breadcrumbs indiquant l'avancement de l'instruction """
        breadcrumbs = [["description de la manifestation", 0]]
        if self.formulaire_complet():
            breadcrumbs = [["description de la manifestation", 1]]
        if hasattr(self, 'charte'):
            breadcrumbs.append(["charte d'engagement", 1])
        else:
            breadcrumbs.append(["charte d'engagement", 0])
        if  not self.finalise:
            breadcrumbs.append(["demande non envoyée", 0])
        else:
            breadcrumbs.append(["demande envoyée", 1])
            if not self.dossier_complet():
                breadcrumbs.append(["dossier à complèter", 0])
        return breadcrumbs

    def formulaire_complet(self):
        """ Indiquer si le formulaire est complet """
        for champ in self.CHAMPS_REQUIS:
            if getattr(self, champ) == None:
                return False
            elif isinstance(getattr(self, champ), str) and getattr(self, champ) == '':
                return False
            elif isinstance(getattr(self, champ), models.Manager):
                return getattr(self, champ).all().exists()

        return True

    def dossier_complet(self):
        """ Indiquer si le dossier est complet en fonction de la date courante et des pièces manquantes """
        if not self.formulaire_complet():
            return False
        return True

    def liste_manquants(self):
        """
        Donner la liste des documents manquants en fonction de la date courante
        On détermine le modèle de dossier puis on retrouve le champ dans le méta du modèle pour extraire le verbose_name
        """
        listemanquants = []
        if not self.formulaire_complet():
            listemanquants.append('formulaire incomplet')
        return listemanquants

    def get_docs_complementaires_manquants(self):
        """ Renvoyer les documents complémentaires dont le fichier n'est pas renseigné """
        return self.documentscomplementaires.filter(document_attache='')

    def liste_referents(self):
        recipients = list(Referent.objects.filter(user__default_instance__departement__isnull=True))
        recipients += Referent.objects.filter(user__default_instance__departement=self.ville_depart.arrondissement.departement)
        return recipients

    def ajouter_document(self):
        """ Avertir et enregistrer l'action """
        origine = self.get_organisateur()
        Action.objects.log(origine, "document ajouté au dossier", self)
        recipients = self.liste_referents()
        Notification.objects.notify_and_mail(recipients, "document ajouté au dossier", self.structure, self)

    def envoyer(self):
        """ Avertir et enregistrer l'action """
        origine = self.get_organisateur()
        Action.objects.log(origine, "déclaration envoyée au référent", self)
        recipients = self.liste_referents()
        Notification.objects.notify_and_mail(recipients, "déclaration envoyée au référent", self.structure, self)

    # Meta
    class Meta:
        verbose_name = "manifestation"
        verbose_name_plural = "manifestations"
        default_related_name = "manifs"
        app_label = "evenements"


class ManifRelatedModel(models.Model):
    """
    Tous les modèles liés à manifestation peuvent hériter de cette classe

    afin de bénéficier de méthodes liées aux informations de manifestation
    """

    # Getter
    def get_manif(self):
        """ Renvoyer la manifestation de l'objet """
        return self.manif

    def get_instance(self):
        """ Renvoyer l'instance liée à la manifestation liée à l'objet """
        return self.get_manif().get_instance() or Instance.objects.get_master()

    # Méta
    class Meta:
        abstract = True
