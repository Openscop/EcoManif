# coding: utf-8
from django.core.files.storage import default_storage
from django.db import models

from configuration.directory import UploadPath
from evenements.models.manifestation import Manif, ManifRelatedModel
from notifications.models.action import Action
from notifications.models.notification import Notification


class DocumentComplementaire(ManifRelatedModel):
    """ Document supplémentaire pour une manifestation """

    # Champs
    manif = models.ForeignKey(Manif, on_delete=models.CASCADE)
    information_requise = models.TextField("information requise")
    document_attache = models.FileField(upload_to=UploadPath('additional_data'), blank=True, null=True, verbose_name="Document", max_length=512)

    # Override
    def __str__(self):
        """ Renvoyer la représentation texte de l'objet """
        return self.information_requise

    # Action
    def log_doc_fourni(self):
        """ Enregistrer dans le journal la provision de données """
        action = "Documents complémentaires envoyés"
        Action.objects.log(self.manif.structure.organisateur, action, self.manif)

    def log_doc_demande(self, origine):
        """ Enregistrer dans le journal la demande de document """
        action = "Documents complémentaires requis"
        Action.objects.log(origine, action, self.manif)

    def notifier_doc_fourni(self):
        """ Notifier la provision de document """
        subject = "Documents complémentaires envoyés"
        recipients = self.manif.liste_referents()
        Notification.objects.notify_and_mail(recipients, subject, self.manif.structure, self.manif)

    def notifier_doc_demande(self, origine):
        """ Notifier la demande de document """
        subject = "Documents complémentaires requis "
        recipients = self.manif.structure.organisateur
        Notification.objects.notify_and_mail(recipients, subject, origine, self.manif)

    def existe(self):
        """ Renvoyer si le fichier de la pièce jointe existe sur le système de fichiers """
        field_populated = self.id and self.document_attache and self.document_attache.path
        file_exists = default_storage.exists(self.document_attache.name)
        return field_populated and file_exists

    # Meta
    class Meta:
        verbose_name = "document complémentaire"
        verbose_name_plural = "documents complémentaires"
        default_related_name = "documentscomplementaires"
        app_label = "evenements"
