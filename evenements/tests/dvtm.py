# coding: utf-8

from .base import EvenementsTestsBase
from ..factories.manif import DvtmFactory


class DvtmTests(EvenementsTestsBase):
    """ Tests Dvtm """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation compétitive
        sportive motorisée soumise à déclaration.
        """
        self.manifestation = DvtmFactory.create()
        super().setUp()

    def test_get_absolute_url(self):
        """ Tester les URL d'accès """
        manifestation = DvtmFactory.build(pk=4)
        self.assertEqual(manifestation.get_absolute_url(), '/Dvtm/4/')

    def test_display_natura2000_eval_panel(self):
        """ Vérifier que le panneau d'évaluation N2K doit s'afficher dans les cas suivants """
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = False
        self.manifestation.gros_titre = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_titre = False
        self.manifestation.vtm_natura2000 = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())

    def test_legal_delay_for_declared(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 60)
