from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.firefox.webdriver import WebDriver
from selenium.common.exceptions import NoSuchElementException
from core.models import Instance
from django.contrib.auth.hashers import make_password as make
from allauth.account.models import EmailAddress
from django.test import tag
from django.conf import settings
import os, time

from core.factories import UserFactory
from administrative_division.factories import DepartementFactory, ArrondissementFactory, CommuneFactory
from organisateurs.factories import StructureFactory, OrganisateurFactory
from sports.factories import ActiviteFactory
from evenements.models import Manif


class TestAiguillageDcnm(StaticLiveServerTestCase):
    """
    Test du circuit organisateur avec sélénium
        workflow_GGD : Avis GGD + préavis EDSR
        instruction par arrondissement
        openrunner false par défaut

    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if settings.DEBUG == False:
            settings.DEBUG = True

    fixtures = ['socialapp']
    DELAY = 0.4

    @classmethod
    def setUpClass(cls):
        """
        Préparation du test
            Création du driver sélénium
            Création des objets nécessaires au formulaire de création de manifestation
            Création des fichiers nécessaires à joindre à la création de la manifestation
        """
        print()
        print('========== Aiguillage DCNM ==========')
        cls.selenium = WebDriver()
        cls.selenium.implicitly_wait(15)
        # IMPORTANT pour la bonne marche du test, permet de dégager le footer
        cls.selenium.set_window_size(600, 800)

        cls.dep = DepartementFactory.create(name='42',
                                            instance__name="instance de test",
                                            instance__workflow_ggd=Instance.WF_GGD_SUBEDSR,
                                            instance__instruction_mode=Instance.IM_ARRONDISSEMENT)
        arrondissement = ArrondissementFactory.create(name='Montbrison', code='98', departement=cls.dep)
        cls.commune = CommuneFactory(name='Bard', arrondissement=arrondissement)
        cls.autrecommune = CommuneFactory(name='Roche', arrondissement=arrondissement)

        cls.organisateur = UserFactory.create(username='organisateur',
                                              email='organisateur@example.com',
                                              password=make(123),
                                              default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.organisateur, email='organisateur@example.com', primary=True, verified=True)
        organisateur = OrganisateurFactory.create(user=cls.organisateur)
        cls.instructeur = UserFactory.create(username='instructeur',
                                             email='instructeur@example.com',
                                             password=make(123),
                                             default_instance=cls.dep.instance)
        EmailAddress.objects.create(user=cls.instructeur, email='instructeur@example.com', primary=True, verified=True)

        cls.structure = StructureFactory(commune=cls.commune, organisateur=organisateur)
        cls.activ = ActiviteFactory.create()

        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'certificat_organisateur_tech', 'commissaires', 'itineraire_horaire', 'topo_securite'):
            mon_fichier = open(file+".txt", "w")
            mon_fichier.write("Document Manifestation Sportive")
            mon_fichier.write("")
            mon_fichier.write("Test du circuit Organisateur")
            mon_fichier.close()

        super(TestAiguillageDcnm, cls).setUpClass()

    @classmethod
    def tearDownClass(cls):
        """
        Nettoyage après test
            Suppression des fichiers créés dans /tmp
            Arrêt du driver sélénium
        """
        os.chdir("/tmp")
        for file in ('reglement_manifestation', 'engagement_organisateur', 'disposition_securite', 'cartographie',
                     'certificat_organisateur_tech', 'commissaires', 'itineraire_horaire', 'topo_securite'):
            os.remove(file+".txt")

        cls.selenium.quit()
        super(TestAiguillageDcnm, cls).tearDownClass()

    def chosen_select(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.find_element_by_css_selector("a").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    def chosen_select_multi(self, id_chosen, value):
        """
        Sélectionner une valeur dans une entrée de type select modifiée par chosen
        :param id_chosen: identification de la div chosen
        :param value: entrée à sélectionner
        """
        chosen_select = self.selenium.find_element_by_id(id_chosen)
        chosen_select.find_element_by_css_selector("input").click()
        time.sleep(self.DELAY)
        results = chosen_select.find_elements_by_tag_name('li')
        for result in results:
            if value in result.text:
                result.click()

    @tag('selenium')
    def test_Dcnm(self):
        """
        Circuit organisateur pour une déclaration sans véhicules
        :return:
        """
        print('**** test aiguillage DCNM ****')
        # Connexion
        self.selenium.get('%s%s' % (self.live_server_url, '/accounts/login/'))
        pseudo_input = self.selenium.find_element_by_name("login")
        pseudo_input.send_keys('organisateur')
        time.sleep(self.DELAY)
        password_input = self.selenium.find_element_by_name("password")
        password_input.send_keys('123')
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
        time.sleep(self.DELAY*4)
        self.assertIn(('Tableau de bord organisateur'), self.selenium.page_source)

        self.selenium.find_element_by_partial_link_text('Déclarer ou demander').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_partial_link_text('Accéder au formulaire').click()
        # remplir formulaire d'aiguillage
        self.chosen_select('id_discipline_chosen', 'Discipline')
        time.sleep(self.DELAY)
        self.chosen_select('id_activite_chosen', 'Activite')
        time.sleep(self.DELAY)
        self.chosen_select('id_departement_chosen', '42')
        time.sleep(self.DELAY)
        self.chosen_select('id_ville_depart_chosen', 'Bard')
        time.sleep(self.DELAY)
        entries = self.selenium.find_element_by_id('id_nb_participants')
        entries.send_keys('150')
        self.selenium.find_element_by_xpath("//span[contains(text(),'Se déroule')]").click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_xpath("//span[contains(text(),'Classement')]").click()
        time.sleep(self.DELAY)

        self.selenium.find_element_by_xpath("//button[contains(text(),'Rechercher')]").click()
        time.sleep(self.DELAY)
        # Tester bon aiguillage
        self.assertIn('Dcnm', self.selenium.page_source)
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()

        self.assertIn('Organisation d\'une manifestation sportive', self.selenium.page_source)

        # Remplir formulaire manifestation
        name = self.selenium.find_element_by_id('id_nom')
        name.send_keys('La grosse course')

        description = self.selenium.find_element_by_id('id_description')
        description.send_keys('une grosse course qui déchire')
        time.sleep(self.DELAY)

        self.chosen_select_multi('id_villes_traversees_chosen', 'Roche')
        time.sleep(self.DELAY)

        parcours = self.selenium.find_element_by_id('id_descriptions_parcours')
        parcours.send_keys('parcours de 10Km')

        audience = self.selenium.find_element_by_id('id_nb_spectateurs')
        audience.send_keys('100')
        time.sleep(self.DELAY)

        vehicles = self.selenium.find_element_by_id('id_nb_vehicules_accompagnement')
        vehicles.send_keys('1')
        time.sleep(self.DELAY)

        signaleurs = self.selenium.find_element_by_id('id_nb_signaleurs')
        signaleurs.send_keys('1')
        time.sleep(self.DELAY)

        nom_contact = self.selenium.find_element_by_id('id_nom_contact')
        nom_contact.send_keys('durand')
        time.sleep(self.DELAY)

        prenom_contact = self.selenium.find_element_by_id('id_prenom_contact')
        prenom_contact.send_keys('joseph')
        time.sleep(self.DELAY)

        tel_contact = self.selenium.find_element_by_id('id_tel_contact')
        tel_contact.send_keys('0605555555')
        time.sleep(self.DELAY)

        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        time.sleep(self.DELAY)

        print('**** test étape 1 ****')
        # Tester la présence de la page détail
        self.assertIn('une grosse course qui déchire', self.selenium.page_source)
        # Tester le status de la manifestation
        boutons = self.selenium.find_elements_by_css_selector('.page-header li')

        try:
            boutons[0].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape1 n\'est pas vert")
        try:
            boutons[1].find_element_by_class_name('fa-exclamation-triangle')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape2 n\'est pas rouge")
        # Tester les fichiers manquants
        card = self.selenium.find_element_by_xpath("//div[@class='card-header bg-primary']/parent::*")
        self.assertIn('Liste des pièces manquantes', card.text)
        files = card.find_elements_by_css_selector('li')
        self.assertEqual(5, len(files))
        for file in files:
            if file.text not in ('réglement de la manifestation', 'déclaration et engagement de l\'organisateur',
                                 'dispositions prises pour la sécurité', 'itinéraire horaire',
                                 'cartographie de parcours'):
                self.assertTrue(False, msg="le fichier "+file.text+" n\'est pas dans la liste")

        self.selenium.find_element_by_partial_link_text('Joindre des documents').click()

        print('**** test ajout fichiers ****')
        self.assertIn('Fichiers joints', self.selenium.page_source)

        manif = Manif.objects.get()
        manif.openrunner_route = (456132,)
        manif.save()

        file1 = self.selenium.find_element_by_id('id_reglement_manifestation')
        file1.send_keys('/tmp/reglement_manifestation.txt')
        file2 = self.selenium.find_element_by_id('id_engagement_organisateur')
        file2.send_keys('/tmp/engagement_organisateur.txt')
        file3 = self.selenium.find_element_by_id('id_disposition_securite')
        file3.send_keys('/tmp/disposition_securite.txt')
        file8 = self.selenium.find_element_by_id('id_cartographie')
        file8.send_keys('/tmp/cartographie.txt')
        file4 = self.selenium.find_element_by_id('id_itineraire_horaire')
        file4.send_keys('/tmp/itineraire_horaire.txt')
        self.selenium.find_element_by_xpath('//input[@type="submit"]').click()

        self.selenium.find_element_by_partial_link_text('Envoyer la demande').click()

        # self.selenium.find_element_by_xpath('//input[@type="submit"]').click()
        # time.sleep(self.DELAY)

        print('**** test étape 2 ****')
        # Retour en page détail
        self.assertIn('une grosse course qui déchire', self.selenium.page_source)
        # Tester le status
        boutons = self.selenium.find_elements_by_css_selector('.page-header li')
        try:
            boutons[0].find_element_by_class_name('fa-check')
        except NoSuchElementException:
            self.assertTrue(False, msg="Le bouton Etape1 n\'est pas vert")
        # try:
        #     boutons[1].find_element_by_class_name('fa-check')
        # except NoSuchElementException:
        #     self.assertTrue(False, msg="Le bouton Etape2 n\'est pas vert")

        self.selenium.find_element_by_class_name('navbar-toggler').click()
        time.sleep(self.DELAY)
        self.selenium.find_element_by_class_name('deconnecter').click()
        # time.sleep(self.DELAY)
        # self.selenium.find_element_by_xpath('//button[@type="submit"]').click()
