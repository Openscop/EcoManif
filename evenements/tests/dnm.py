# coding: utf-8
from evenements.factories import DnmFactory
from evaluations.factories import Natura2000EvalManifFactory
from evaluations.factories import RNREvalManifFactory
from .base import EvenementsTestsBase


class DnmTests(EvenementsTestsBase):
    """ Tests Dnm """

    def setUp(self):
        """
        La manifestation créée ici est une manifestation
        sportive non motorisée soumise à déclaration.
        """
        self.manifestation = DnmFactory.create()
        super().setUp()

    def test_str(self):
        self.assertEqual(str(self.manifestation), self.manifestation.nom.title())

    def test_display_rnr_eval_panel(self):
        """ Tester la méthode indiquant s'il faut afficher le panneau d'évaluation RNR """
        self.manifestation.dans_rnr = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_rnr())
        self.manifestation.dans_rnr = False
        self.assertFalse(self.manifestation.afficher_panneau_eval_rnr())

    def test_display_natura2000_eval_panel(self):
        self.manifestation.lucratif = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.lucratif = False
        self.assertFalse(self.manifestation.afficher_panneau_eval_n2000())
        self.manifestation.gros_budget = True
        self.assertTrue(self.manifestation.afficher_panneau_eval_n2000())

    def test_legal_delay(self):
        self.assertEqual(self.manifestation.get_delai_legal(), 30)

    def test_get_breadcrumbs(self):
        self.assertEqual(len(self.manifestation.get_breadcrumbs()), 2)
        self.assertEqual(self.manifestation.get_breadcrumbs()[1][1], 0)

    def test_get_breadcrumbs_full(self):
        RNREvalManifFactory.create(manif=self.manifestation)
        Natura2000EvalManifFactory.create(manif=self.manifestation)
        self.assertEqual(len(self.manifestation.get_breadcrumbs()), 4)

    def test_get_breadcrumbs_full_2(self):
        self.manifestation.dans_rnr = True
        self.manifestation.lucratif = True
        self.assertEqual(len(self.manifestation.get_breadcrumbs()), 4)
