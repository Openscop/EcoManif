# coding: utf-8
from ajax_select.helpers import make_ajax_form
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin
from django.urls import reverse
from django.utils.html import format_html

from core.util.admin import RelationOnlyFieldListFilter
from evenements.admin.filter import CalendrierFilter, AVenirFilter
from evenements.admin.inline import DocumentComplementaireInline, CharteInLine
from evenements.models.manifestation import Manif
from evenements.models.sansinstance import SansInstanceManif


@admin.register(Manif)
class ManifAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations """

    # Configuration
    list_display = ('nom', 'date_creation', 'structure', 'activite__name', 'date_debut',
                    'ville_depart__arrondissement__departement', 'finalise')
    list_filter = [CalendrierFilter, AVenirFilter,
                   ('ville_depart__arrondissement__departement', RelationOnlyFieldListFilter), 'activite__name']
    readonly_fields = ['date_creation']
    search_fields = ['nom']
    inlines = [DocumentComplementaireInline, CharteInLine]
    list_per_page = 25
    ordering = ['-date_creation']
    list_display_links = ['nom']

    fieldsets = (
        (None, {'fields': ('instance', 'nom', ('date_creation', 'date_debut', 'date_fin'), 'description', 'finalise',
                           ('structure', 'activite'), ('nb_participants', 'nb_spectateurs'))}),
        ('Géographie', {'classes': ('collapse',),
                        'fields': (('ville_depart', 'parcs', 'villes_traversees', 'departements_traverses'),)}),
        ('Calendrier', {'classes': ('collapse',),
                        'fields': (('prive', 'cache', 'afficher_adresse_structure'))}),
        ('fichiers', {'classes': ('collapse',),
                      'fields': ('cartographie', 'convention', 'docs_additionels')}),
        ('Contact', {'classes': ('collapse',),
                     'fields': (('prenom_contact', 'nom_contact', 'email_contact', 'tel_contact'),)}),
    )
    form = make_ajax_form(Manif, {'ville_depart': 'commune', 'villes_traversees': 'commune',
                                  'departements_traverses': 'departement', 'structure': 'structure'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser or request.user.get_instance().name == '(Master)':
            return queryset
        return queryset.filter(ville_depart__arrondissement__departement=request.user.get_departement())


@admin.register(SansInstanceManif)
class SansInstanceManifAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des manifestations sans instance """

    # Configuration
    list_display = ('nom', 'structure', 'activite__name', 'date_debut', 'ville_depart__arrondissement__departement')
    list_filter = [('ville_depart__arrondissement__departement', RelationOnlyFieldListFilter), 'activite__name']
    readonly_fields = ['date_creation']
    search_fields = ['nom']
    list_per_page = 25
    ordering = ['-date_creation']

    fieldsets = (
        (None, {'fields': ('instance', 'nom', ('date_creation', 'date_debut', 'date_fin'), 'description',
                           ('structure', 'activite'), ('nb_participants', 'nb_spectateurs'))}),
        ('Géographie', {'classes': ('collapse',),
                        'fields': (('ville_depart', 'parcs', 'villes_traversees', 'departements_traverses'),)}),
        ('Contact', {'classes': ('collapse',),
                     'fields': (('prenom_contact', 'nom_contact', 'tel_contact'),)}),
    )
    form = make_ajax_form(SansInstanceManif, {'ville_depart': 'commune', 'villes_traversees': 'commune',
                                              'departements_traverses': 'departement', 'structure': 'structure'})

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser or request.user.get_instance().name == '(Master)':
            return queryset
        return queryset.filter(ville_depart__arrondissement__departement=request.user.get_departement())
