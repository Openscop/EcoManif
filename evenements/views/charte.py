from django.utils import timezone
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView, View
from django.views.generic.edit import UpdateView, CreateView
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.http import FileResponse

from core.util.permissions import require_role
from organisateurs.decorators import verifier_proprietaire
from ..models import Charte, Manif
from ..forms.charte import CharteForm
from configuration.directory import Paths


@method_decorator(require_role('organisateur'), name='dispatch')
class CharteCreate(CreateView):
    """ Création de manifestation """

    # Configuration
    model = Charte
    form_class = CharteForm

    # Overrides
    def form_valid(self, form):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        form.instance.manif = manif
        user = manif.structure.organisateur.user
        manif.actions.create(user=user, action="questionnaire de la charte rempli")
        return super().form_valid(form)

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['manif_pk'] = self.kwargs['manif_pk']
        return kwargs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['manif'] = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        return context

    def get_success_url(self):
        return reverse('evenements:manif_detail', kwargs={'pk': self.object.manif.pk})


class CharteUpdate(UpdateView):
    """ Modifier une manifestation """

    # Configuration
    model = Charte
    form_class = CharteForm

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # Vérifier si la manifestation est finalisée
        if manif.finalise:
            return render(self.request, "core/notification.html",
                          {'title': "Editer la charte", 'message': "L'édition n'est plus possible !", 'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        return super().dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        return manif.charte

    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        kwargs['manif_pk'] = self.kwargs['manif_pk']
        return kwargs

    def get_success_url(self):
        return reverse('evenements:manif_detail', kwargs={'pk': self.object.manif.pk})


class CharteDiplome(TemplateView):
    """ Page de remise du diplôme """

    template_name = 'evenements/charte_diplome.html'

    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # Vérifier si la manifestation est finalisée
        if not manif.finalise:
            return render(self.request, "core/notification.html",
                          {'title': "Editer le diplôme", 'message': "Le diplôme est accessible seulement quand la déclaration est finalisée !", 'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        elif manif.date_fin < timezone.now():
            return render(self.request, "core/notification.html",
                          {'title': "Editer le diplôme",
                           'message': "La manifestation est manitenant terminée !",
                           'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        elif manif.charte.diplome() == "echec":
            return render(self.request, "core/notification.html",
                          {'title': "Editer le diplôme",
                           'message': "La manifestation n'a pas obtenu une note suffisante à l'obtention d'un diplôme !",
                           'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        context['manif'] = manif
        context['diplome'] = manif.charte.diplome()
        context['date'] = timezone.now()
        return context


class CharteImpression(TemplateView):
    """ Page de présentation de la charte pour impression """

    template_name = 'evenements/charte_impression.html'

    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # Vérifier si la manifestation est finalisée
        if not manif.finalise:
            return render(self.request, "core/notification.html",
                          {'title': "Imprimer la charte", 'message': "La charte est accessible seulement quand la déclaration est finalisée !", 'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        elif manif.date_fin < timezone.now():
            return render(self.request, "core/notification.html",
                          {'title': "Imprimer la charte",
                           'message': "La manifestation est manitenant terminée !",
                           'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        context['manif'] = manif
        context['diplome'] = manif.charte.diplome()
        context['date'] = timezone.now()
        return context


class CharteLabel(View):
    """ Vue de téléchargement des labels """

    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        # Vérifier si la manifestation est finalisée
        if not manif.finalise:
            return render(self.request, "core/notification.html",
                          {'title': "Editer le diplôme", 'message': "Le diplôme est accessible seulement quand la déclaration est finalisée !", 'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        elif manif.date_fin < timezone.now():
            return render(self.request, "core/notification.html",
                          {'title': "Editer le diplôme",
                           'message': "La manifestation est manitenant terminée !",
                           'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        elif manif.charte.diplome() == "echec":
            return render(self.request, "core/notification.html",
                          {'title': "Editer le diplôme",
                           'message': "La manifestation n'a pas obtenu une note suffisante à l'obtention d'un diplôme !",
                           'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        return super().dispatch(*args, **kwargs)

    def get(self, request, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        diplome = manif.charte.diplome()
        file = Paths.get_root_dir('portail', 'static', 'portail', 'img')
        if diplome == 'or':
            file += 'Or'
        elif diplome == 'argent':
            file += 'Argent'
        else:
            file += 'Bronze'
        if self.kwargs['label'] == 'blanc':
            file += '.jpg'
        else:
            file += '.png'
        return FileResponse(open(file, 'rb'), as_attachment=True)
