# coding: utf-8
from django.contrib.messages.views import SuccessMessageMixin
from django.shortcuts import get_object_or_404
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse_lazy

from core.util.permissions import require_role
from ..forms.documentcomplementaire import DocumentComplementaireRequestForm, DocumentComplementaireProvideForm
from ..models import *


class DocumentComplementaireRequestView(SuccessMessageMixin, CreateView):
    """ Demande de documents complémentaires """

    # Configuration
    model = DocumentComplementaire
    form_class = DocumentComplementaireRequestForm
    success_message = "Demande de documents complémentaires envoyée avec succès"

    # Overrides
    @method_decorator(require_role(['referent']))
    def dispatch(self, *args, **kwargs):
        return super(DocumentComplementaireRequestView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        manif = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        form.instance.manif = manif
        reponse = super(DocumentComplementaireRequestView, self).form_valid(form)
        form.instance.log_doc_demande(self.request.user)
        form.instance.notifier_doc_demande(self.request.user)
        return reponse

    def get_context_data(self, **kwargs):
        context = super(DocumentComplementaireRequestView, self).get_context_data(**kwargs)
        context['manif'] = get_object_or_404(Manif, pk=self.kwargs['manif_pk'])
        return context

    def get_success_url(self):
        return reverse('referents:manif_detail-referent', kwargs={'pk': self.object.manif.pk})


class DocumentComplementaireProvideView(SuccessMessageMixin, UpdateView):
    """ Renseignement de documents complémentaires """

    # Configuration
    model = DocumentComplementaire
    form_class = DocumentComplementaireProvideForm
    success_message = "Documents complémentaires envoyés avec succès"
    success_url = reverse_lazy('evenements:tableau-de-bord-organisateur')

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        return super(DocumentComplementaireProvideView, self).dispatch(*args, **kwargs)

    def form_valid(self, form):
        reponse = super(DocumentComplementaireProvideView, self).form_valid(form)
        form.instance.log_doc_fourni()
        form.instance.notifier_doc_fourni()
        return reponse
