# coding: utf-8
from django.forms.fields import IntegerField
from django.forms.models import ModelChoiceField
from django.forms.widgets import HiddenInput
from django.http import HttpResponseRedirect
from django.shortcuts import render, reverse, redirect, get_object_or_404
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.views.generic import DetailView, CreateView, TemplateView
from django.views.generic.edit import UpdateView

from administrative_division.models.commune import Commune
from administrative_division.models.departement import Departement
from core.util.user import UserHelper
from ..forms.manif import ManifInstructeurUpdateForm
from organisateurs.decorators import verifier_proprietaire
from core.util.permissions import require_role
from ..models import Manif
from ..forms.manif import ManifForm, ManifSendForm, ManifFilesForm


class PreambuleCreationEvenementView(TemplateView):
    template_name = 'evenements/preambule_creation.html'


@method_decorator(verifier_proprietaire, name='dispatch')
class ManifDetail(DetailView):
    """ Détail d'une manifestation """

    model = Manif
    template_name = 'evenements/evenement_detail.html'

    # Overrides
    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        # Pour gérer les actions
        context['doc'] = False
        context['requis'] = False
        context['action'] = ""
        if self.object.date_fin < timezone.now():
            context['action'] = "rien"
        else:
            if self.object.finalise:
                context['action'] = "diplome"
                if self.object.get_docs_complementaires_manquants().exists():
                    context['doc'] = True
            else:
                context['requis'] = True
                if not hasattr(self.object, 'charte'):
                    context['action'] = "charte"
                elif self.object.charte.diplome() == 'echec':
                    context['action'] = "echec"
                else:
                    if self.object.dossier_complet():
                        if self.object.delai_depasse():
                            context['action'] = "contact"
                        else:
                            context['action'] = "envoi"
                    else:
                        context['action'] = "manquants"
        # Pour les barres de progression
        if hasattr(self.object, 'charte'):
            diplome = self.object.charte.diplome()
            if diplome == "or":
                context['bg_bar_charte'] = "bg-bar-or"
            elif diplome == "argent":
                context['bg_bar_charte'] = "bg-bar-arg"
            elif diplome == "bronze":
                context['bg_bar_charte'] = "bg-bar-bro"
            else:
                context['bg_bar_charte'] = "bg-secondary progress-bar-striped"
            bg_bar_grp = []
            score_grp = []
            for item in range(0,5):
                niveau = self.object.charte.niveau_grp()[item]
                if niveau == 'oko':
                    bg_bar_grp.append("bg-bar-or")
                    score_grp.append("or")
                elif niveau == 'oka':
                    bg_bar_grp.append("bg-bar-arg")
                    score_grp.append("argent")
                elif niveau == 'okb':
                    bg_bar_grp.append("bg-bar-bro")
                    score_grp.append("bronze")
                else:
                    bg_bar_grp.append("bg-secondary")
                    score_grp.append("")
            context['bg_bar_grp'] = bg_bar_grp
            context['score_grp'] = score_grp

        return context


@method_decorator(require_role('organisateur'), name='dispatch')
class ManifCreate(CreateView):
    """ Création de manifestation """

    # Configuration
    model = Manif
    form_class = ManifForm
    template_name = 'evenements/evenement_form.html'

    # Overrides
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.instance = self.object.ville_depart.get_instance()
        self.object.structure = self.request.user.organisateur.structure
        user = self.object.structure.organisateur.user
        self.object.save()
        form.save_m2m()
        self.object.actions.create(user=user, action="description de la manifestation")
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        request_departement = self.request.GET.get('dept')
        kwargs = super().get_form_kwargs()
        kwargs['extradata'] = request_departement
        self.request.session['extradata'] = request_departement
        self.request.session.save()
        if request_departement:
            kwargs['initial'].update({'departement_depart': Departement.objects.get_by_name(request_departement).pk})
        return kwargs

    def get_success_url(self):
        return reverse('evenements:charte_add', kwargs={'manif_pk': self.object.id})

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        form.target = form.instance
        # Remplacer les champs commune de départ et département si le département est passé dans l"URL
        if request_departement:
            form.fields['departements_traverses'].queryset = Departement.objects.exclude(name=request_departement)
            form.fields['ville_depart'] = ModelChoiceField(required=True, queryset=Commune.objects.by_departement_name(request_departement),
                                                             label="Commune de départ", disabled=True)
            form.fields['departement_depart'] = IntegerField(widget=HiddenInput())
            form._meta.exclude += ('departement_depart',)
        return form

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['type'] = self.model._meta.model_name
        context['verbose'] = self.model._meta.verbose_name
        return context


class ManifUpdate(UpdateView):
    """ Modifier une manifestation """

    # Configuration
    model = Manif
    form_class = ManifForm
    template_name = 'evenements/evenement_form.html'

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['pk'])
        # Vérifier si la manifestation est finalisée
        if manif.finalise:
            return render(self.request, "core/notification.html",
                          {'title': "Editer la déclaration", 'message': "L'édition n'est plus possible !", 'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        return super().dispatch(*args, **kwargs)

    def get_form_kwargs(self):
        request_departement = self.request.GET.get('dept')
        kwargs = super().get_form_kwargs()
        kwargs['extradata'] = request_departement
        self.request.session['extradata'] = request_departement
        self.request.session.save()
        kwargs['initial'].update({'departement_depart': self.object.ville_depart.get_departement().pk})
        # Pour afficher les communes traversées du département de départ, il faut ajouter le département de départ à la liste des département traversés
        # Cela doit venir de clever-select
        # le kwarg écrase la valeur initiale tirée de la DB d'où l'ajout à la liste
        dept_trav = list(self.object.departements_traverses.all())
        dept_trav.append(self.object.ville_depart.get_departement())
        kwargs['initial'].update({'departements_traverses': dept_trav})
        # On transforme le texte des routes/parcours en liste*
        return kwargs

    def get_form(self, form_class=None):
        request_departement = self.request.GET.get('dept')
        form = super().get_form(form_class)
        form.target = form.instance
        form.request = self.request

        # Remplacer les champs commune de départ et département si le département est passé dans l"URL
        if request_departement:
            form.fields['departements_traverses'].queryset = Departement.objects.exclude(name=request_departement)
            form.fields['ville_depart'] = ModelChoiceField(required=True, queryset=Commune.objects.by_departement_name(request_departement),
                                                             label="Commune de départ", disabled=True)
            form.fields['departement_depart'] = IntegerField(widget=HiddenInput())
            form._meta.exclude += ('departement_depart',)
            departement = Departement.objects.get_by_name(request_departement)
            instance = departement.get_instance()
        return form


class ManifSend(UpdateView):
    """ Modifier une manifestation """

    # Configuration
    model = Manif
    form_class = ManifSendForm
    template_name = 'evenements/evenement_form.html'

    # Overrides
    @method_decorator(verifier_proprietaire)
    def dispatch(self, *args, **kwargs):
        manif = get_object_or_404(Manif, pk=self.kwargs['pk'])
        # Si la déclaration existe déjà pour la manifestation, rediriger vers le détail de la manif
        if manif.finalise:
            return render(self.request, "core/notification.html",
                          {'title': "Envoyer la déclaration", 'message': "L'action a déjà été faite !", 'type': 1,
                           'suite': 'evenements:tableau-de-bord-organisateur'})
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['envoyer'] = True
        return context

    def form_valid(self, form):
        manif = get_object_or_404(Manif, pk=self.kwargs['pk'])
        if manif.finalise:
            return redirect(self.get_success_url())
        # Sinon enregister la nouvelle déclaration
        form.instance.finalise = True
        response = super().form_valid(form)
        form.instance.envoyer()
        return response

    def get_success_url(self):
        return reverse('evenements:manif_detail', kwargs={'pk': self.object.id})


@method_decorator(verifier_proprietaire, name='dispatch')
class ManifFilesUpdate(UpdateView):
    """ Modifier une manifestation """

    # Configuration
    model = Manif
    form_class = ManifFilesForm
    template_name = 'evenements/evenement_form.html'

    # Overrides
    def get_form_kwargs(self):
        kwargs = super().get_form_kwargs()
        return kwargs

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.target = form.instance
        # Si la manifestation est finalisée
        # que le champ fichier est rempli alors plus de changement possible
        if form.instance.finalise:
            for field in form.fields:
                if form.initial[field]:
                    form.fields[field].disabled = True
        return form

    def form_valid(self, form):
        """ Le formulaire est valide, gérer la réponse HTTP """
        if form.instance.finalise and form.changed_data:
            for change in form.changed_data:
                if form.cleaned_data[change]:
                    form.instance.ajouter_document()
        return super().form_valid(form)


@method_decorator(require_role(['referent']), name='dispatch')
class ManifInstructeurUpdate(UpdateView):
    """ Formulaire instructeur """

    model = Manif
    form_class = ManifInstructeurUpdateForm
    template_name = 'evenements/evenement_form.html'

    # Overrides
    def get_success_url(self):
        """ URL lors de la validation du formulaire """
        return reverse("referents:manif_detail-referent", kwargs={'pk': self.object.instruction.id})

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        form.fields['afficher_adresse_structure'].required = False
        return form


class ManifPublicDetail(DetailView):
    """ Détails publics d'une manifestation """

    # Configuration
    model = Manif
    template_name = 'evenements/evenementpublic_detail.html'

    def dispatch(self, request, *args, **kwargs):
        instance = self.get_object()
        if not instance.visible_dans_calendrier():
            if not UserHelper.has_role(request.user, 'referent'):
                if instance.structure.organisateur.user != request.user:
                    return render(request, "core/notification.html",
                                  {'title': "Accès restreint", 'type': 1,
                                   'message': "Vous n'avez pas les droits d'accès à cette page"})
        return super().dispatch(request, *args, **kwargs)
