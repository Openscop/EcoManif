# coding: utf-8
from django.http import HttpResponseRedirect
from django.utils.decorators import method_decorator
from django.views.generic.edit import CreateView
from django.forms.models import ModelChoiceField

from administrative_division.models.departement import Departement
from administrative_division.models.commune import Commune
from evenements.forms.sansinstance import SansInstanceManifForm
from organisateurs.decorators import *
from ..models.sansinstance import SansInstanceManif


class SansInstanceManifCreate(CreateView):
    """ Création de manifestation dans un département pas encore configuré """

    # Configuration
    template_name = 'evenements/evenement_sansinstance_form.html'
    model = SansInstanceManif
    form_class = SansInstanceManifForm

    # Overrides
    @method_decorator(require_role('organisateur'))
    def dispatch(self, *args, **kwargs):
        """ N'autoriser l'accès qu'aux organisateurs """
        return super().dispatch(*args, **kwargs)

    def get_form(self, form_class=None):
        """ Renvoyer le formulaire initialisé """
        request_departement = self.request.GET.get('dept')
        departement = Departement.objects.get_by_name(request_departement)
        form = super().get_form(form_class)
        if request_departement:
            form.fields['activite'].initial = self.request.session['activite']
            form.fields['nb_participants'].initial = self.request.session['nb_participants']
            form.fields['departement_depart'].initial = departement.id
            form.fields['departements_traverses'].queryset = Departement.objects.exclude(name=request_departement)
            form.fields['ville_depart'] = ModelChoiceField(required=True, queryset=Commune.objects.by_departement_name(request_departement),
                                                             label="Commune de départ", disabled=True)
            form.fields['ville_depart'].initial = self.request.session['commune']
        return form

    def form_valid(self, form):
        """ Méthode appelée lorsque le formulaire est valide, renvoie une réponse HTTP """
        self.object = form.save(commit=False)
        self.object.structure = self.request.user.organisateur.structure
        self.object.instance = None
        self.object = form.save()
        return HttpResponseRedirect(self.get_success_url())

    def get_form_kwargs(self):
        """ Renvoyer les kwargs utilisés pour initialiser le formulaire """
        kwargs = super().get_form_kwargs()
        self.request.session['extradata'] = ""
        return kwargs
