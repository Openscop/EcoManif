from crispy_forms.layout import Layout, Fieldset, HTML, Submit
from django.urls import reverse
from django import forms

from core.forms.base import GenericForm
from evenements.models import Charte


# Widgets
FORM_WIDGETS = {
    'autre_01': forms.Textarea(attrs={'rows': 1}),
    'autre_02': forms.Textarea(attrs={'rows': 1}),
    'autre_11': forms.Textarea(attrs={'rows': 1}),
    'autre_12': forms.Textarea(attrs={'rows': 1}),
    'autre_21': forms.Textarea(attrs={'rows': 1}),
    'autre_22': forms.Textarea(attrs={'rows': 1}),
    'autre_31': forms.Textarea(attrs={'rows': 1}),
    'autre_32': forms.Textarea(attrs={'rows': 1}),
    'autre_41': forms.Textarea(attrs={'rows': 1}),
    'autre_42': forms.Textarea(attrs={'rows': 1}),
}


class CharteForm(GenericForm):
    """
    Formulaire de la charte ecomanif
    """

    # Overrides
    def __init__(self, *args, **kwargs):
        self.manif_pk = kwargs.pop('manif_pk', None)
        super().__init__(*args, **kwargs)
        self.helper.label_class = 'col-sm-1'
        self.helper.field_class = 'col-sm-11'
        self.helper.layout = Layout(HTML("<p>Vous pouvez enregistrer ce formulaire à tout moment puis le reprendre ultérieurement.<br>Ce pictogramme <i class='feuille'></i> signale une action obligatoire.</p><hr>"),
                                    HTML('<div id="charteformaccordion"> <div class="card bg-light">'),
                                    HTML('<div class="card-header" id="charteform1"> <div class="btn btn-break" data-toggle="collapse" data-target="#collapsecharteform1" aria-expanded="false" aria-controls="collapsecharteform1"><h1 class="bg-group1"><i class="charte_grp0"></i><strong>' +
                                         Charte.CHARTE_GROUP[0] + '</strong></h1></div></div><div id="collapsecharteform1" class="collapse" aria-labelledby="charteform1" data-parent="#charteformaccordion">'),
                                    Fieldset('',
                                             HTML("<br>"),
                                             *Charte.CHARTE_GROUP_MEMBER[0],
                                             css_class="bg-group1"),
                                    HTML('</div>'),
                                    HTML('<div class="card-header" id="charteform2"> <div class="btn btn-break" data-toggle="collapse" data-target="#collapsecharteform2" aria-expanded="false" aria-controls="collapsecharteform2"><h1 class="bg-group2"><i class="charte_grp1"></i></i><strong>' +
                                         Charte.CHARTE_GROUP[1] + '</strong></h1></div></div><div id="collapsecharteform2" class="collapse" aria-labelledby="charteform2" data-parent="#charteformaccordion">'),
                                    Fieldset('',
                                             HTML("<h2 class='pl-5 bg-group2'>Déchets</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[1][:5],
                                             HTML("<h2 class='pl-5 bg-group2'>Alimentation</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[1][5:10],
                                             HTML("<h2 class='pl-5 bg-group2'>Matériel</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[1][10:],
                                             css_class="bg-group2"),
                                    HTML('</div>'),
                                    HTML('<div class="card-header" id="charteform3"> <div class="btn btn-break" data-toggle="collapse" data-target="#collapsecharteform3" aria-expanded="false" aria-controls="collapsecharteform3"><h1 class="bg-group3"><i class="charte_grp2"></i></i><strong>' +
                                         Charte.CHARTE_GROUP[2] + '</strong></h1></div></div><div id="collapsecharteform3" class="collapse" aria-labelledby="charteform3" data-parent="#charteformaccordion">'),
                                    Fieldset('',
                                             HTML("<h2 class='pl-5 bg-group3'>Communication</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[2][:6],
                                             HTML("<h2 class='pl-5 bg-group3'>Sensibilisation</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[2][6:],
                                             css_class="bg-group3"),
                                    HTML('</div>'),
                                    HTML('<div class="card-header" id="charteform4"> <div class="btn btn-break" data-toggle="collapse" data-target="#collapsecharteform4" aria-expanded="false" aria-controls="collapsecharteform4"><h1 class="bg-group4"><i class="charte_grp3"></i></i><strong>' +
                                         Charte.CHARTE_GROUP[3] + '</strong></h1></div></div><div id="collapsecharteform4" class="collapse" aria-labelledby="charteform4" data-parent="#charteformaccordion">'),
                                    Fieldset('',
                                             HTML("<h2 class='pl-5 bg-group4'>Transports et la mobilité durable</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[3][:7],
                                             HTML("<h2 class='pl-5 bg-group4'>Energie et changements climatiques</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[3][7:],
                                             css_class="bg-group4"),
                                    HTML('</div>'),
                                    HTML('<div class="card-header" id="charteform5"> <div class="btn btn-break" data-toggle="collapse" data-target="#collapsecharteform5" aria-expanded="false" aria-controls="collapsecharteform5"><h1 class="bg-group5"><i class="charte_grp4"></i></i><strong>' +
                                         Charte.CHARTE_GROUP[4] + '</strong></h1></div></div><div id="collapsecharteform5" class="collapse" aria-labelledby="charteform5" data-parent="#charteformaccordion">'),
                                    Fieldset('',
                                             HTML("<h2 class='pl-5 bg-group5'>Les bénévoles et riverains</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[4][:3],
                                             HTML("<h2 class='pl-5 bg-group5'>L’accessibilité et solidarité</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[4][3:7],
                                             HTML("<h2 class='pl-5 bg-group5'>Promotion de la santé</h2>"),
                                             *Charte.CHARTE_GROUP_MEMBER[4][7:],
                                             css_class="bg-group5"),
                                    HTML('</div></div></div>'),
                                    Submit('save', 'Enregistrer', css_class="btn-half mt-4"),
                                    HTML('<a class="btn btn-danger btn-half mt-4 float-right" href="' + reverse('evenements:manif_detail', kwargs={'pk': self.manif_pk}) + '">Annuler</a>'),
                                    )
        for cle in Charte.CHARTE_MEMBER:
            if Charte.CHARTE_MEMBER.get(cle)[0]:
                field = self.fields[cle]
                css = field.widget.attrs.get('class', '')
                field.widget.attrs['class'] = 'requis ' + css

    # Meta
    class Meta:
        model = Charte
        exclude = ['manif']
        widgets = FORM_WIDGETS
