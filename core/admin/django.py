# coding: utf-8

from ckeditor_uploader.fields import RichTextUploadingFormField
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.flatpages.admin import FlatPageAdmin, FlatpageForm as BaseFlatpageForm
from django.contrib.flatpages.models import FlatPage
from django.utils.translation import ugettext_lazy as _

from core.admin.filters import RoleFilter
from core.models.user import User
from core.models.instance import Instance
from core.forms import CustomUserChangeForm, CustomUserCreateForm

class FlatpageForm(BaseFlatpageForm):
    """ Formulaire admin des flat pages """

    # Initialiser
    def __init__(self, *args, **kwargs):
        return super().__init__(*args, **kwargs)

    # Champs
    content = RichTextUploadingFormField()


class FlatPageAdmin(FlatPageAdmin):
    """ Administration des flatpages """

    # Configuration
    form = FlatpageForm


class UtilisateurAdmin(UserAdmin):
    """ Configuration de l'admin utilisateur """
    form = CustomUserChangeForm
    add_form = CustomUserCreateForm

    # Configuration
    list_display = ('username', 'email', 'is_active', 'is_staff', 'date_joined', 'default_instance', 'get_role', 'get_service')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups', 'default_instance', RoleFilter)
    readonly_fields = ('get_service', 'get_role',)
    ordering = ('-date_joined',)
    fieldsets = (
        (None, {'fields': ('username', 'password', ('default_instance', 'get_role', 'get_service'))}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    list_per_page = 25

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser or request.user.get_instance().name == '(Master)':
            return queryset
        return queryset.filter(default_instance__departement=request.user.get_departement())

    def save_model(self, request, obj, form, change):
        """ Enregistrer le modèle """
        try:
            instance = request.user.get_instance()
            if not obj.default_instance and not instance.is_master():
                obj.default_instance = instance
        except (AttributeError, Exception):
            pass
        return super().save_model(request, obj, form, change)

    def get_form(self, request, obj=None, **kwargs):
        """ Empêcher les non superuser de modifier les permissions des utilisateurs """
        if not request.user.is_superuser:
            # Vous ne voulez surtout pas donner accès à ces 3 champs
            self.exclude = ['user_permissions', 'is_superuser']
            self.fieldsets = (
                (None, {'fields': ('username', 'password', ('default_instance', 'get_role', 'get_service'))}),
                (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
                (_('Permissions'), {'fields': ('is_active', 'is_staff', 'groups')}),
                (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
            )
        form = super().get_form(request, obj=obj, **kwargs)
        if request.user.get_departement():
            if hasattr(form.base_fields, 'default_instance'):
                form.base_fields['default_instance'].queryset = Instance.objects.filter(departement=request.user.get_departement())
                form.base_fields['default_instance'].initial = request.user.get_instance()
        return form


admin.site.unregister(FlatPage)

admin.site.register(User, UtilisateurAdmin)
admin.site.register(FlatPage, FlatPageAdmin)
