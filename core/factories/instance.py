# coding: utf-8
import factory
from factory.django import DjangoModelFactory

from administrative_division.factories import DepartementFactory
from core.models.instance import Instance


class InstanceFactory(DjangoModelFactory):
    """ Factory des instances normales """

    # Champs
    name = factory.Sequence(lambda n: "Configuration GGD Sub-EDSR {0}".format(n))
    departement = factory.SubFactory(DepartementFactory)
    workflow_ggd = Instance.WF_GGD_SUBEDSR

    # Meta
    class Meta:
        model = Instance


class MasterInstanceFactory(DjangoModelFactory):
    """ Factory des instances normales """

    # Champs
    departement = None
    name = "Configuration master"

    # Meta
    class Meta:
        model = Instance


class InstanceAFactory(DjangoModelFactory):
    """ Factory d'instance avec paramètres spécifiques """

    # Champs
    name = "Configuration GGD Sub-EDSR A"
    workflow_ggd = Instance.WF_GGD_SUBEDSR
    manifestation_delay = 47
    manifestation_anm_1dept_delay = 47
    manifestation_anm_ndept_delay = 0  # utiliser la valeur de Instance.objects.get_master
    manifestation_dvtm_delay = 0
    manifestation_avtmc_delay = 0
    manifestation_avtm_vp_delay = 0
    manifestation_avtm_vn_delay = 0

    # Meta
    class Meta:
        model = Instance
