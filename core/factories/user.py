# coding: utf-8
import factory
from django.contrib.auth import get_user_model


class UserFactory(factory.django.DjangoModelFactory):
    """ Factory utilisateur """

    # Champs
    username = factory.Sequence(lambda n: 'jdoe{0}'.format(n))
    email = factory.Sequence(lambda n: 'john.doe{0}@example.com'.format(n))

    # Meta
    class Meta:
        model = get_user_model()
