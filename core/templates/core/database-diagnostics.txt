État des bases de données
==========================
{% for database, state in data.items %}
    Base de données {{ database }}
    =====================================
    {% for model, count in state.items %}
    {{ model | ljust:40 }} : {{ count }}{% endfor %}
{% endfor %}
