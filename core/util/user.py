# coding: utf-8
"""
Utilitaires pour les utilisateurs

UserHelper : cette classe permet plusieurs choses :
    - récupérer le type d'agent correspondant au rôle d'un utilisateur
    - récupérer le service attaché à un utilisateur. ex.: s'il est agent GGD, renvoyer son GGD
"""
from collections import OrderedDict
from operator import itemgetter


class UserHelper:
    """ Utilitaires liés aux utilisateurs """

    # Constantes
    ROLE_TYPES = ['organisateur', 'editeur', 'referent']
    DIRECT_TYPES = ['organisateur', 'editeur', 'referent']
    ROLE_CHOICES = [['organisateur', "Organisateur"], ['editeur', "Editeur"], ['referent', "Référent"]]

    SERVICES = {'editeur': 'ddcs', 'referent': 'ddcs', 'organisateur': 'structure'}
    ROLE_CHOICES = sorted(ROLE_CHOICES, key=itemgetter(1))
    ROLE_CHOICES_DICT = OrderedDict(ROLE_CHOICES)

    # Fonctions
    @staticmethod
    def get_role_names(user):
        """ Renvoie les chaînes correspondant aux rôles de l'utilisateur """
        roles = []
        for role in UserHelper.ROLE_TYPES:
            # Tester les rôles dont le modèle est lié directement à User
            try:
                if getattr(user, role) is not None:
                    roles.append(role)
            except AttributeError:
                pass
            # Tester le nom de modèle pour user.agent (uniquement avec parent_link=True)
            try:
                model_name = user.agent._meta.model_name
                if model_name in UserHelper.ROLE_TYPES:
                    roles.append(model_name)
            except AttributeError:
                pass
            # Tester le nom de modèle pour user.agentlocal (uniquement avec parent_link=True)
            try:
                model_name = user.agentlocal._meta.model_name
                if model_name in UserHelper.ROLE_TYPES:
                    roles.append(model_name)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agent (déprécie depuis parent_link)
            try:
                if getattr(user.agent, role) is not None:
                    roles.append(role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agentlocal (déprécié depuis parent_link)
            try:
                if getattr(user.agentlocal, role) is not None:
                    roles.append(role)
            except AttributeError:
                pass
        return roles

    @staticmethod
    def get_role_name(user):
        """ Renvoyer un nom de rôle pour l'utilisateur """
        roles = UserHelper.get_role_names(user)
        return roles[0] if roles else None

    @staticmethod
    def get_role_instance(user):
        """ Renvoyer l'objet de rôle pour l'utilisateur """
        for role in UserHelper.ROLE_TYPES:
            # Tester les rôles dont le modèle est lié directement à User.agent
            try:
                if getattr(user.agent, role) is not None:
                    return getattr(user.agent, role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User.agentlocal
            try:
                if getattr(user.agentlocal, role) is not None:
                    return getattr(user.agentlocal, role)
            except AttributeError:
                pass
            # Tester les rôles dont le modèle est lié directement à User
            try:
                if getattr(user, role) is not None:
                    return getattr(user, role)
            except AttributeError:
                pass
        # Si les autres méthodes ne marchent pas, on suppose que parent_link a été utilisé
        try:
            return user.agentlocal
        except AttributeError:
            pass
        try:
            return user.agent
        except AttributeError:
            pass
        return None

    @staticmethod
    def has_role(user, role):
        """ Renvoyer si l'utilisateur possède un rôle, donné par son nom par UserHelper.ROLE_TYPES """
        # Tester les rôles dont le modèle est lié directement à User
        if user.is_anonymous:
            return False
        try:
            if getattr(user, role) is not None:
                return True
        except AttributeError:
            pass
        # Tester les rôles dont le modèle est lié directement à User.agent
        try:
            if getattr(user.agent, role) is not None:
                return True
        except AttributeError:
            pass
        # Tester les rôles dont le modèle est lié directement à User.agentlocal
        try:
            if getattr(user.agentlocal, role) is not None:
                return True
        except AttributeError:
            pass
        return False

    @staticmethod
    def get_service_instance(user):
        """ Renvoyer l'instance de service correspondant à l'utilisateur """
        role_instance = UserHelper.get_role_instance(user)
        role_field = UserHelper.get_role_name(user)
        service_field = UserHelper.SERVICES.get(role_field, None)
        try:
            if service_field is not None and role_instance is not None:
                return getattr(role_instance, service_field)
            else:
                return None
        except AttributeError:
            return None
