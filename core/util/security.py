# coding: utf-8
from django.conf import settings
from django.core.exceptions import ImproperlyConfigured


def protect_code():
    """ Lève une exception ImproperlyConfigured lorsque settings.PROTECT_CODE == True """
    code_protected = getattr(settings, 'PROTECT_CODE', False)
    if code_protected is True:
        raise ImproperlyConfigured("Vous ne pouvez pas exécuter ce code, car il est marqué comme protégé.")
