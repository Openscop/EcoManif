# coding: utf-8
from django import template

register = template.Library()

@register.filter
def verbose_name(obj):
    if obj:
        return obj._meta.verbose_name
    else:
        return ""


@register.filter
def verbose_name_plural(obj):
    if obj:
        return obj._meta.verbose_name_plural
    else:
        return ""