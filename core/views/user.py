# coding: utf-8
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import DetailView, TemplateView
from django.views.generic.edit import UpdateView
from allauth.account.views import SignupView
from django.contrib.auth.views import LogoutView
from django.contrib.auth import get_user_model

from core.forms import UserUpdateForm, SignupOrganisateurForm
from nouveautes.models import Nouveaute

class ProfileDetailView(DetailView):
    """ Vue du profil utilisateur """

    # Configuration
    model = get_user_model()

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(ProfileDetailView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['news']= bool(Nouveaute.objects.for_user(self.request.user))
        return context


class UserUpdateView(UpdateView):
    """ Modification du profil utilisateur """

    # Configuration
    model = get_user_model()
    form_class = UserUpdateForm
    success_url = '/accounts/profile/'

    # Overrides
    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super(UserUpdateView, self).dispatch(*args, **kwargs)

    def get_object(self, queryset=None):
        return self.request.user


class SignupOrganisateurView(SignupView):
    form_class = SignupOrganisateurForm
    view_name = 'signuporganisateurview'

    def get_context_data(self, **kwargs):
        ret = super(SignupOrganisateurView, self).get_context_data(**kwargs)
        ret.update(self.kwargs)
        return ret

    def get_form(self, form_class=None):
        form = super().get_form(form_class)
        # on insere request dans form pour utilisation dans clean_data
        form.request = self.request
        # Si un utilisateur se connecte par un compte tiers, il est déjà identifié, on récupère ses données
        if self.request.user.is_authenticated:
            form.fields['last_name'].initial = self.request.user.last_name
            form.fields['last_name'].widget.attrs={'readonly': True}
            form.fields['first_name'].initial = self.request.user.first_name
            form.fields['first_name'].widget.attrs={'readonly': True}
            form.fields['email'].initial = self.request.user.email
            form.fields['email'].widget.attrs = {'readonly': True}
        return form


signuporganisateurview = SignupOrganisateurView.as_view()


class EmailConfirmedView(TemplateView):
    template_name = "account/email_confirmed.html"

    def get_context_data(self, **kwargs):
        context = super(EmailConfirmedView, self).get_context_data(**kwargs)
        user = self.request.user
        context['user'] = user
        return context


class CustomLogoutView(LogoutView):
    template_name = "account/logout.html"

    def dispatch(self, request, *args, **kwargs):
        if 'provider' in self.request.session:
            self.provider = self.request.session['provider']
            if self.provider == 'franceconnect':
                self.state = self.request.session['state']
                self.token = self.request.session['id_token']
        ret = super().dispatch(request, *args, **kwargs)
        return ret

    def get_context_data(self, **kwargs):
        cont = super().get_context_data(**kwargs)
        if hasattr(self, 'provider'):
            cont['provider'] = self.provider
            if self.provider == 'franceconnect':
                cont['state'] = self.state
                cont['token'] = self.token
        return cont