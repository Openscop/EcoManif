# coding: utf-8
import simplejson

from django.conf import settings
from django.forms.models import ModelForm
from django.http.response import HttpResponse, HttpResponseBadRequest
from django.views.decorators.http import require_POST

from core.util.shortcuts import import_fullname
from core.util.types import make_iterable


@require_POST
def validate_form(request, form_classes=None, alias=None):
    """
    Vue de validation de formulaire (AJAX)

    Valider ou non un formulaire et renvoyer des données AJAX des erreurs
    :param request: Requête HTTP (WSGI)
    :param form_classes: classe de formulaire à valider
    :param alias: alias de classes de formulaire (voir settings.FORM_ALIASES)
    La validation AJAX frontend se fait via le plugin jQuery.LiveValidation.js
    (se trouve dans static/tool/jQuery/One). La page appelle liveValidate sur un
    élément formulaire.
    Le paramètre settings.FORM_ALIASES est de la forme :
    - Dictionnaire {alias: [FQN de classes de formulaires]}
    - Dictionnaire {alias: FQN de classe de formulaire}

    :returns: Des données au format JSON. La clé 'valid' indique si la validation
        s'est faite correctement. Si la valeur de 'valid' est False, alors d'autres
        clés contiendront des données d'erreur. La clé '_all_' est une liste
        contenant les erreurs générales de validation. Les autres clés sont
        les id des champs qui ont provoqué l'échec de la validation, leur valeur
        contient le message d'erreur
    """
    # Initialisation
    if form_classes:
        forms = form_classes
    elif alias:
        aliases = getattr(settings, 'FORM_ALIASES', dict())
        form_names = make_iterable(aliases.get(alias))
        forms = [import_fullname(form_name) for form_name in form_names if '.' in form_name]
    else:
        return HttpResponseBadRequest("You need to send a form or a form alias")
    # Vérifier la validité de tous les formulaires passés
    forms = make_iterable(forms)
    output = {'valid': True, '_all_': []}
    id_passed = request.POST.get('id')
    for form in forms:
        instance = None
        if id_passed and issubclass(form, ModelForm):
            klass = form._meta.model
            instance = klass.objects.get(id=id_passed)
        form = form(request.POST, request.FILES, instance=instance)
        # Vérifier la validité du formulaire
        if not form.is_valid():
            output['_all_'].append(form.non_field_errors())
            output['valid'] = False
            field_names = form.fields.keys()
            for field_name in field_names:
                auto_id = form[field_name].auto_id
                output[auto_id] = form[field_name].errors
        else:
            pass
    return HttpResponse(simplejson.dumps(output))
