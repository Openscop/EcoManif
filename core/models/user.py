# coding: utf-8
from django.contrib.auth.models import AbstractUser, UserManager as BaseUserManager
from django.db import models
from django.utils.html import format_html

from core.models.instance import Instance
from core.util.admin import set_admin_info
from core.util.user import UserHelper


class UserManager(BaseUserManager):
    """ Manager des utilisateurs """

    # Getter
    def active(self):
        """ Renvoyer les utilisateurs actifs """
        return self.filter(is_active=True)

    def agents(self, name):
        """
        Renvoyer des agents départementaux

        :param name: type d'agent à renvoyer, ex. 'ggdagent'
        """
        query = {'agent__{0}__isnull'.format(name): False}
        return self.filter(**query)

    def agentslocaux(self, name):
        """
        Renvoyer des agents locaux

        :param name: type d'agent à renvoyer, ex. 'cgdagentlocal'
        """
        query = {'agentlocal__{0}__isnull'.format(name): False}
        return self.filter(**query)

    def by_role(self, name):
        """
        Renvoyer des utilisateurs qui ont un rôle particulier

        :param name: ex. 'instructeur'
        :type name: str
        """
        query = {'{0}__isnull'.format(name): False}
        return self.filter(**query)


class User(AbstractUser):
    """ Modèle utilisateur """

    # Constantes
    STATUS = [[0, "Normal"], [1, "Nom d'utilisateur modifié"]]
    NORMAL, USERNAME_CHANGED = 0, 1

    # Champs
    default_instance = models.ForeignKey('core.instance', null=True, related_name='users', verbose_name="Instance de configuration", on_delete=models.SET_NULL)
    status = models.SmallIntegerField(choices=STATUS, default=NORMAL, verbose_name="Statut")
    objects = UserManager()

    # Getter
    def get_instance(self):
        """ Renvoyer l'instance de configuration de workflow """
        return self.default_instance or Instance.objects.get_master()

    def get_departement(self):
        """ Renvoyer le département de l'instance de l'utilisateur """
        return self.get_instance().get_departement()

    @set_admin_info(short_description="Service")
    def get_service(self):
        """ Renvoyer le service de l'utilisateur """
        return UserHelper.get_service_instance(self)

    @set_admin_info(short_description="Rôles")
    def get_role(self):
        """ Renvoyer l'objet role de l'utilisateur """
        roles = UserHelper.get_role_names(self)
        if len(roles):
            list = []
            for role in roles:
                role_instance = getattr(self, role)
                display = type(role_instance)._meta.object_name
                url = role_instance.get_url()
                list.append("<a href=" + url +">" + display + "</a>")
            return format_html(' , '.join(list))
        return None

    def has_role(self, role):
        """ Renvoyer si l'utilisateur possède un rôle, donné par son nom par UserHelper.ROLE_TYPES """
        return UserHelper.has_role(self, role)

    @set_admin_info(short_description="Rôles")
    def get_friendly_role_names(self):
        """ Renvoyer le nom des rôles pour l'utilisateur """
        roles = UserHelper.get_role_names(self)
        return [UserHelper.ROLE_CHOICES_DICT[name] for name in roles]

    @set_admin_info(short_description="Nom complet")
    def get_full_name(self):
        """ Renvoyer le nom complet de l'utilisateur """
        full_name = "{0} {1}".format(self.first_name.title(), self.last_name.title()).strip()
        return full_name or self.username

    # Setter
    def set_instance(self, instance):
        """
        Définir l'instance par défaut de l'utilisateur

        :param instance: nom du département ou instance (ex. '42') ou instance
        :type instance: core.models.Instance | str
        :return: True si l'opération a correctement été effectuée
        :rtype: bool
        """
        if isinstance(instance, str):
            instances = Instance.objects.filter(departement__name=instance)
            if instances.exists():
                self.default_instance = instances.first()
                return True
            return False
        else:
            self.default_instance = instance
            return True

    # Méta
    class Meta:
        verbose_name = "Utilisateur"
        verbose_name_plural = "Utilisateurs"
        app_label = 'core'


class OneToUserModelMixin:
    """ Mixin pour les clés naturelles des objets liés en 1:1 aux utilisateurs """

    # Overrides
    def natural_key(self):
        return self.user.username,


class OneToUserQuerySetMixin:
    """ Mixin pour les clés naturelles des objets liés en 1:1 aux utilisateurs """

    # Overrides
    def get_by_natural_key(self, username):
        return self.get(user__username=username)
