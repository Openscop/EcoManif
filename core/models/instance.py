# coding: utf-8
""" Configuration """
from datetime import timedelta
from smtplib import SMTPException

import logging
from django.conf import settings
from django.core.mail import get_connection, send_mail
from django.db import models
from django.db.utils import IntegrityError

from administrative_division.models.departement import Departement
from core.util.admin import set_admin_info
from core.util.application import get_current_subdomain


logger = logging.getLogger('core')
mail_logger = logging.getLogger('smtp')


class InstanceManager(models.Manager):
    """ Manager pour les instances """

    # Getter
    def get_for_request(self, request):
        """ Renvoyer l'objet d'instance pour l'utilisateur """
        subdomain = get_current_subdomain(request)
        try:
            number = subdomain.strip()
            instance = self.get(departement__name=number)
            return instance
        except (ValueError, Instance.DoesNotExist):
            # Si le sous-domaine n'est pas valide, utiliser l'instance de l'utilisateur
            if hasattr(request, 'user') and request.user.is_authenticated:
                instance = request.user.get_instance()
                return instance
            return self.get_master()

    def get_master(self):
        """ Renvoyer l'instance par défaut """
        masters = self.filter(departement__isnull=True)
        if masters.exists():
            return masters.first()
        return self.create(departement=None, name="(Master)")

    def not_master(self):
        """ Renvoyer les instances non master """
        return self.exclude(departement__isnull=True).order_by("name")

    def configured(self):
        """ Renvoyer toutes les instances non master """
        return self.not_master()

    @staticmethod
    def get_for_user(user):
        """ Renvoyer l'instance de configuration utilisée par l'utilisateur """
        return user.get_instance()

    def get_by_natural_key(self, departement):
        return self.get(departement=departement)

    # Setter
    def set_for_departement(self, number):
        """ Inscrire un département : assigner à une nouvelle instance (et/ou renvoyer l'instance) """
        try:
            departement = Departement.objects.db_manager(self.db).get(name=number)
            instance = self.get_or_create(departement=departement)[0]
            if not instance.name:
                instance.name = "Configuration {num}".format(num=number)
                instance.color = "f2f2f2"
                instance.save()
            return instance
        except IntegrityError:
            return self.get(departement__name=number)


class Instance(models.Model):
    """ Configuration de l'application par département """

    # Constantes
    INSTRUCTION_MODES = [[0, "Par arrondissement"], [1, "Par département"]]
    IM_ARRONDISSEMENT, IM_DEPARTEMENT = 0, 1
    EMAIL_HELP = """Paramètres SMTP au format host;port;user;password. Une chaîne vide utilise les paramètres par défaut."""

    # Champs
    name = models.CharField(max_length=96, blank=False, verbose_name="Nom")
    departement = models.OneToOneField('administrative_division.departement', null=True, related_name='instance', verbose_name="Département", on_delete=models.SET_NULL)
    color = models.CharField(max_length=8, blank=True, help_text="ex. FF0000", verbose_name="Code couleur HTML")

    # Modes d'instruction (territoire)
    instruction_mode = models.SmallIntegerField(default=IM_DEPARTEMENT, choices=INSTRUCTION_MODES, verbose_name="Mode d'instruction", help_text="L'option \"Par département\" permet à tous les instructeurs de toutes les préfecture & sous-préfectures du département d'être concernés par toutes les manifestations du département. L'option \"Par arrondissement\" oriente les manifestations vers la préfecture ou sous-préfectures de l'arrondissement.")
    manifestation_delay = models.SmallIntegerField(default=15, verbose_name="Délai manifestation")

    # Scoring
    group1_argent = models.SmallIntegerField(default=5, verbose_name="Seuil argent du groupe 1")
    group1_or = models.SmallIntegerField(default=6, verbose_name="Seuil or du groupe 1")
    group2_argent = models.SmallIntegerField(default=8, verbose_name="Seuil argent du groupe 2")
    group2_or = models.SmallIntegerField(default=10, verbose_name="Seuil or du groupe 2")
    group3_argent = models.SmallIntegerField(default=8, verbose_name="Seuil argent du groupe 3")
    group3_or = models.SmallIntegerField(default=11, verbose_name="Seuil or du groupe 3")
    group4_argent = models.SmallIntegerField(default=5, verbose_name="Seuil argent du groupe 4")
    group4_or = models.SmallIntegerField(default=8, verbose_name="Seuil or du groupe 4")
    group5_argent = models.SmallIntegerField(default=4, verbose_name="Seuil argent du groupe 5")
    group5_or = models.SmallIntegerField(default=6, verbose_name="Seuil or du groupe 5")
    global_argent = models.SmallIntegerField(default=30, verbose_name="Seuil argent du diplôme")
    global_or = models.SmallIntegerField(default=41, verbose_name="Seuil or du diplôme")

    # Email d'expéditeur pour les notifications de manifestations
    sender_email = models.CharField(max_length=128, default=settings.DEFAULT_FROM_EMAIL, verbose_name="Email d'expéditeur")
    email_settings = models.CharField(max_length=192, blank=True, default='', help_text=EMAIL_HELP, verbose_name="Configuration email")

    objects = InstanceManager()

    # Getter
    def is_master(self):
        """
        Renvoie si cette instance sert de modèle lorsqu'une information est indisponible

        dans une instance de département.
        """
        return self.departement is None

    def get_email(self):
        """ Renvoie l'email d'expéditeur de l'instance """
        return self.sender_email

    @set_admin_info(short_description="Couleur")
    def get_default_color(self):
        """ Renvoyer une couleur qui correspond au nom de l'objet """
        return "#{0}".format(hex(hash(self.name.lower()))[-6:])

    @set_admin_info(short_description="Mode d'instruction")
    def get_instruction_mode(self):
        """ Renvoyer le mode d'instruction """
        return self.instruction_mode

    def get_departement(self):
        """ Renvoyer le département """
        return self.departement

    def get_departement_name(self):
        """ Renvoyer le nom du département, ou le territoire """
        name = self.departement.name if self.departement else ""
        return name.upper()

    def get_departement_friendly_name(self):
        """ Renvoyer le nom entier du département """
        name = self.departement.get_name_display() if self.departement else "France"
        return name

    def get_manifestation_delay(self):
        """ Renvoyer le délai ou celui par défaut """
        return self.manifestation_delay or Instance.objects.get_master().manifestation_delay

    def get_email_settings(self):
        """ Renvoyer les paramètres d'email pour l'instance """
        values = self.email_settings
        if values.strip() == '' or len(values.split(';')) < 4:
            values = "{host};{port};{user};{pwd}".format(host=settings.EMAIL_HOST, port=int(settings.EMAIL_PORT), user=settings.EMAIL_HOST_USER,
                                                         pwd=settings.EMAIL_HOST_PASSWORD)
        values = values.split(';')
        config = {'host': values[0], 'port': int(values[1]), 'username': values[2], 'password': values[3]}
        return config

    def get_color(self):
        """ Renvoyer la couleur de l'instance """
        code = self.color.replace('#', '')
        return "#{0}".format(code or "FAFAFA")

    # Actions
    def send_test_mail(self):
        """ Envoyer un mail de test avec les paramètres de l'instance """
        sender_email = self.get_email() or settings.DEFAULT_FROM_EMAIL
        email_config = self.get_email_settings()
        connection = get_connection(use_tls=True, **email_config)  # utiliser le EMAIL_BACKEND
        try:
            send_mail(subject="Message de test Manifestationsportive.fr", message="Ceci est un message de test", from_email=sender_email,
                      recipient_list=['destinataire@aa.aa'], connection=connection)
        except SMTPException:
            mail_logger.exception("core.instance.send_test_mail: failure")
        return True

    # Override
    def save(self, *args, **kwargs):
        if self.pk is None and self.departement is None:
            # Forcer l'unicité de l'instance master
            count = Instance.objects.filter(departement=None).count()
            if count != 0:
                raise IntegrityError("Une seule instance master (number=None) peut exister")
        return super(Instance, self).save(*args, **kwargs)

    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        name = self.name or self.departement.get_name_display() if self.departement else "Master"
        departement_name = self.departement.name if self.departement else "Nationale"
        return "{name} ({dept})".format(name=name, dept=departement_name)

    def natural_key(self):
        return self.departement,

    # Métadonnées
    class Meta:
        verbose_name = "configuration d'instance"
        verbose_name_plural = "configurations d'instance"
        app_label = 'core'
