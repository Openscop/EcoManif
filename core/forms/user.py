# coding: utf-8
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Fieldset, HTML
from django import forms
from allauth.account.forms import SignupForm
from allauth.account.adapter import get_adapter
from allauth.account.utils import setup_user_email
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from django.contrib.auth import get_user_model
from django.contrib.sites.models import Site
from django.urls import reverse_lazy
from django.forms.models import ModelChoiceField
from django.core.mail import send_mail
from django.core.exceptions import ValidationError

from administrative_division.models import Commune
from administrative_division.models.departement import Departement
from clever_selects.form_fields import ChainedModelChoiceField
from core.forms.base import GenericForm
from core.models import User
from core.util.champ import TelField
from organisateurs.models import Organisateur, Structure, StructureType


class UserUpdateForm(GenericForm):
    """ Formulaire de modification de l'utilisateur """

    # Overrides
    def __init__(self, *args, **kwargs):
        super(UserUpdateForm, self).__init__(*args, **kwargs)
        self.helper.add_input(Submit('submit', "Enregistrer"))

    # Meta
    class Meta:
        model = get_user_model()
        fields = ('first_name', 'last_name')


class CustomUserCreateForm(UserCreationForm):

    def clean_username(self):
        uname = self.cleaned_data['username']
        if User.objects.filter(username__iexact=uname):
            raise ValidationError("Le nom d'utilisateur est déjà pris !")
        return uname


class CustomUserChangeForm(UserChangeForm):

    def clean_username(self):
        uname = self.cleaned_data['username']
        if uname != self.initial['username']:
            if User.objects.filter(username__iexact=uname):
                raise ValidationError("Le nom d'utilisateur est déjà pris !")
        return uname

    def clean(self):
        cleaned_data = super(CustomUserChangeForm, self).clean()
        if cleaned_data['is_active'] != self.initial['is_active']:
            if cleaned_data['is_active'] == True:
                send_mail(
                    'Validation d\'inscription',
                    "Mr ou Mme " + self.instance.first_name + '  ' + self.instance.last_name + chr(10) +
                    'Votre compte est maintenant validé sur la plateforme ' + Site.objects.get_current().domain + '.' + chr(10) +
                    "nom d'utilisateur : " + self.instance.username,
                    'plateforme@manifestationsportive.fr',
                    [self.instance.email],
                )


class SignupOrganisateurForm(SignupForm):
    """ Formulaire d'inscription des organisateurs"""

    # Champs
    first_name = forms.CharField(max_length=30, label="Prénom du demandeur")
    last_name = forms.CharField(max_length=30, label="Nom du demandeur")
    cgu = forms.BooleanField(label="En cochant cette case, j'accepte les <a href='/CGU'>Conditions Générales d'Utilisation</a>")

    # Structure
    structure_name = forms.CharField(
        label="Nom de la structure", max_length=200,
        help_text="Précisez le nom de la structure organisatrice (nom de l'association ou du club, de la société ou de la personne physique s'il s'agit d'un "
                  "particulier) de la manifestation"
    )
    type_of_structure = forms.ModelChoiceField(
        label="Forme juridique de la structure", queryset=StructureType.objects.all(),
        help_text="Opérez votre choix parmi les statuts juridiques proposés"
    )
    address = forms.CharField(label="Adresse", max_length=255)
    departement = ModelChoiceField(required=False, queryset=Departement.objects.all(), label="Département")
    commune = ChainedModelChoiceField('departement', reverse_lazy('administrative_division:commune_widget'), Commune, label="Commune", required=False)
    phone = TelField(label="Numéro de téléphone", max_length=14)
    website = forms.URLField(label="Site web", max_length=200, required=False)

    # Overrides
    def __init__(self, *args, **kwargs):
        """ Initialiser l'objet """
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_class = 'form-horizontal'
        self.helper.label_class = 'col-sm-3'
        self.helper.field_class = 'col-sm-6'
        self.fields['username'].help_text = "le nom d'utilisateur vous servira comme identifiant lors de la connexion au site"
        self.fields['username'].label = "Nom d'utilisateur"
        self.helper.layout = Layout(
            Fieldset(
                "Organisateur",
                'first_name',
                'last_name',
                'username',
                'email',
                'password1',
                'password2',
                'cgu',
            ),
            Fieldset(
                "Structure",
                'structure_name',
                'type_of_structure',
                'address',
                'departement',
                'commune',
                'phone',
                'website',
            )
        )
        self.helper.add_input(Submit('submit', "Inscription"))

    def clean_structure_name(self):
        structure_name = self.cleaned_data['structure_name']
        if Structure.objects.filter(name=structure_name).exists():
            raise forms.ValidationError("Le nom \"{name}\" est déjà utilisé.".format(name=structure_name))
        return structure_name

    def clean_first_name(self):
        first_name = self.cleaned_data['first_name']
        if hasattr(self.request.user, 'first_name') and first_name != self.request.user.first_name:
            raise ValidationError("Ce champ doit garder sa valeur initiale !")
        return first_name

    def clean_last_name(self):
        last_name = self.cleaned_data['last_name']
        if hasattr(self.request.user, 'last_name') and last_name != self.request.user.last_name:
            raise ValidationError("Ce champ doit garder sa valeur initiale !")
        return last_name

    def clean_email(self):
        # Si user vient de socialaccount, on ne check pas email
        if self.request.user.is_authenticated:
            email = self.cleaned_data['email']
            if email != self.request.user.email:
                raise ValidationError("Ce champ doit garder sa valeur initiale !")
        else:
            email = super().clean_email()
        return email

    def clean_commune(self):
        commune = self.cleaned_data['commune']
        if not commune:
            raise ValidationError("Ce champ est obligatoire")
        return commune

    def save(self, request):

        # reprise du code de allauth.account.form.save pour modification en lieu et place de super()
        # Si user vient de socialaccount, on n'en crée pas un nouveau
        adapter = get_adapter(request)
        if request.user.is_authenticated:
            user = request.user
        else:
            user = adapter.new_user(request)
        adapter.save_user(request, user, self)
        self.custom_signup(request, user)
        # Si user vient de socialaccount, pas de vérification de l'adresse mail
        if not request.user.is_authenticated:
            setup_user_email(request, user, [])
        # fin de reprise du code

        cgu = self.cleaned_data['cgu']
        organisateur = Organisateur(user=user, cgu=cgu)
        organisateur.save()
        structure = Structure.objects.create(
            name=self.cleaned_data['structure_name'],
            type_of_structure=self.cleaned_data['type_of_structure'],
            phone=self.cleaned_data['phone'],
            website=self.cleaned_data['website'],
            organisateur=organisateur,
            address=self.cleaned_data['address'],
            commune=self.cleaned_data['commune'],
        )
        user.default_instance = structure.commune.get_instance()
        user.save()
        return user
