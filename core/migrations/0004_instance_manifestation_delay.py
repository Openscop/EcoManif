# Generated by Django 2.1.2 on 2019-03-20 15:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0003_auto_20190320_1525'),
    ]

    operations = [
        migrations.AddField(
            model_name='instance',
            name='manifestation_delay',
            field=models.SmallIntegerField(default=30, verbose_name='Délai manifestation'),
        ),
    ]
