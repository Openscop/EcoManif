# Generated by Django 2.2.1 on 2019-10-01 13:41

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0005_auto_20190604_1113'),
    ]

    operations = [
        migrations.AlterField(
            model_name='instance',
            name='sender_email',
            field=models.CharField(default='ne-pas-repondre@ecomanif-sport-auvergne-rhone-alpes.fr', max_length=128, verbose_name="Email d'expéditeur"),
        ),
    ]
