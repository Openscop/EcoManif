# coding: utf-8
"""
Remplacer tous les mots de passe par 123.
Utilisable uniquement en mode DEBUG.
"""
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand
from django.db import transaction

from core.util.security import protect_code


class Command(BaseCommand):
    """ Nettoyer et charger les données par défaut de la base de données """
    args = ''
    help = 'Remplace tous les mots de passe des utilisateurs par le paramètre passé ; defaut, 123'

    def add_arguments(self, parser):
        parser.add_argument('passwd', nargs='?', type=str, default='123')

    def handle(self, *args, **options):
        """ Exécuter la commande """
        protect_code()

        if settings.DEBUG:
            with transaction.atomic():
                confirm = ''
                passwd = options['passwd']
                if (passwd != '123'):
                    print('Les mots de passe seront remplacés par : "' + str( passwd) + '".')
                    print('Entrez Y pour continuer.')
                    confirm = input()
                if (confirm == 'Y' or confirm == 'y' or passwd == '123'):
                    count = get_user_model().objects.count()
                    for user in get_user_model().objects.all():
                        user.set_password(passwd)
                        user.save()
                    print("Les mots de passe de {0} utilisateurs ont été mis à jour.".format(count))
                else:
                    print('Abandon')
        else:
            print("Échec : Par sécurité, vous ne pouvez lancer cette commande qu'en mode DEBUG.")
