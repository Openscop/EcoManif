# coding: utf-8
"""
Lister les communes qui accueil des manifestations sur cette seule commune
(Code postal + ville)
"""
from django.core.management.base import BaseCommand
from evenements.models import Manif
from django.db.models import Count


class Command(BaseCommand):

    args = ''
    help = 'Lister les communes qui accueil des manifestations sur cette seule commune. fichier de sortie : data.txt'

    def handle(self, *args, **options):
        """ Exécuter la commande """

        handle1 = open('data.txt', 'w+')
        handle1.write(str(list(Manif.objects.annotate(nbr_cities=Count('villes_traversees')).filter(nbr_cities=1).values('ville_depart__code', 'ville_depart__name').distinct())))
        handle1.close()
        print("Le fichier a été créé.")
