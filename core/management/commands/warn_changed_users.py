# coding: utf-8
from allauth.account.models import EmailAddress
from django.conf import settings
from django.core.mail import send_mail
from django.core.management.base import BaseCommand
from django.template.loader import render_to_string

from core.models.user import User


class Command(BaseCommand):
    def handle(self, *args, **options):
        """ Envoyer des mails de confirmation aux utilisateurs non confirmés """

        # Envoyer un mail de confirmation pour l'email non validé
        for emailaddress in EmailAddress.objects.filter(verified=False):
            emailaddress.send_confirmation()

        # Envoyer un email pour indiquer le nouveau nom d'utilisateur des utilisateurs modifiés
        for user in User.objects.filter(status=User.USERNAME_CHANGED):
            email = user.email

            data = {'username': user.username, 'url': settings.MAIN_URL}
            subject = "Changement de votre nom d'utilisateur"
            message = render_to_string('core/mail/username-changed.txt', data)
            sender_email = settings.DEFAULT_FROM_EMAIL
            send_mail(subject=subject, message=message, from_email=sender_email, recipient_list=[email])
