from django.db import models
from django.conf import settings
from django.urls import reverse

# Create your models here.
class Referent(models.Model):
    """ Utilisateur qui consultent les manifestations """

    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="referent", verbose_name="utilisateur", on_delete=models.CASCADE)

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.user.get_username()

    def get_url(self):
        """ Renvoyer l'URL de l'objet """
        return reverse('admin:referents_referent_change', args=(self.pk,))

    class Meta:
        verbose_name = "référent"
        verbose_name_plural = "référents"
        app_label = "referents"
        default_related_name = "referents"
