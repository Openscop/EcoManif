from django.apps import AppConfig


class ReferentsConfig(AppConfig):
    """ Configuration d'Application """

    # Configuration
    name = 'referents'
    verbose_name = "referents"


default_app_config = 'referents.ReferentsConfig'
