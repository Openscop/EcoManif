# coding: utf-8
from django.urls import path, re_path

from .views import TableauDeBordReferent, ManifDetailReferent


app_name = 'referents'
urlpatterns = [

    # Tableau de bord principal
    path('tableau-de-bord-referent/', TableauDeBordReferent.as_view(), name="tableau-de-bord-referent"),

    # Manifestation
    re_path('referent/(?P<pk>\d+)/', ManifDetailReferent.as_view(), name="manif_detail-referent"),

]
