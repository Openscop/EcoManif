# coding: utf-8
from django.contrib import admin
from django.contrib.admin.filters import RelatedOnlyFieldListFilter
from django.db.models.aggregates import Count
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from sports.forms import FederationForm
from ..models import Federation


@admin.register(Federation)
class FederationAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Configuration admin """

    list_display = ['id', 'short_name', 'name', 'level', 'departement', 'email', 'multisports', 'disciplines_liées']
    list_editable = []
    list_filter = [('departement', RelatedOnlyFieldListFilter), 'region']
    search_fields = ['name']
    form = FederationForm
    list_per_page = 50

    def disciplines_liées(self, obj):
        return " | ".join([d.name for d in obj.disciplines.all()])

