# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from ..models import Activite
from ..models import Discipline


@admin.register(Discipline)
class DisciplineAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """

    list_display = ['pk', 'name', 'milieu', 'motorise', 'activités_liées']
    list_filter = ['motorise', 'milieu']
    list_editable = ['milieu']
    search_fields = ['name', 'federation__name', 'activites__name']
    list_per_page = 25

    def activités_liées(self, obj):
        return " | ".join([a.name for a in obj.activites.all()])

@admin.register(Activite)
class ActiviteAdmin(ImportExportActionModelAdmin):
    """ Configuration admin """

    list_display = ['pk', 'name', 'discipline']
    list_filter = ['discipline', 'discipline__motorise']
    search_fields = ['name', 'discipline__name']
    list_per_page = 25
