from django.conf import settings
from django.urls import include, path, re_path
from django.conf.urls.static import static

from django.contrib import admin
from django.contrib.sitemaps import views as sitemaps_views
from django.contrib.flatpages.sitemaps import FlatPageSitemap

from core.views.ajax import validate_form
from core.views.user import (ProfileDetailView, UserUpdateView, EmailConfirmedView, CustomLogoutView)
from organisateurs.views import StructureUpdateView
from portail.views.index import HomePage


admin.autodiscover()

sitemaps = {
    'flatpages': FlatPageSitemap,
}
handler400 = 'portail.views.erreur400view'
handler403 = 'portail.views.erreur403view'
handler404 = 'portail.views.erreur404view'
handler500 = 'portail.views.erreur500view'

urlpatterns = [
    # Examples:
    # path('', 'ddcs_loire.views.home', name='home'),
    # path('blog/', include('blog.urls')),

    path('admin/doc/', include('django.contrib.admindocs.urls')),
    path('admin/', admin.site.urls),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('sitemap\.xml', sitemaps_views.sitemap, {'sitemaps': sitemaps}),
    path('accounts/logout/', CustomLogoutView.as_view(), name='logout'),
    path('accounts/', include('allauth.urls')),
    path('accounts/email_confirmed', EmailConfirmedView.as_view(), name='email_confirmed'),
    path('accounts/profile/', ProfileDetailView.as_view(), name='profile'),
    path('accounts/profile/edit', UserUpdateView.as_view(), name='profile_edit'),
    path('accounts/profile/structure/edit', StructureUpdateView.as_view(), name='structure_edit'),
    path('inscription/', include('core.urls')),
    path('nouveautes/', include('nouveautes.urls')),
    path('sports/', include('sports.urls')),
    path('edition/', include('editeurs.urls')),
    path('administrative_division/', include('administrative_division.urls')),
    path('', include('evenements.urls')),
    path('', include('referents.urls')),
    path('parcs_naturels/', include('parcs_naturels.urls')),
    re_path('validate/(?P<alias>[\w\-]+)/', validate_form, name='validate'),
    path('ajax_select/', include('ajax_select.urls')),
    path('', include('portail.urls')),
    path('', HomePage.as_view(), name='home_page'),
    path('eco/', include('aide.urls')),
    path('captcha/', include('captcha.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

urlpatterns += [path('', include('django.contrib.flatpages.urls')),
                ]

if settings.DEBUG:
    import debug_toolbar
    urlpatterns = [path('__debug__/', include(debug_toolbar.urls))] + urlpatterns
