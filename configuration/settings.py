# coding: utf-8
import logging
import sys
from collections import OrderedDict
import yaml
import sentry_sdk
from sentry_sdk.integrations.django import DjangoIntegration

from core.util.migrations import DisableMigrations
from configuration.directory import Paths
from configuration import thirdparty

# Get the local settings of the server
server_settings = {}
try:
    with open('/etc/django/settings-ecomanif.yaml', 'r') as f:
        server_settings = yaml.load(f, Loader=yaml.FullLoader)
except FileNotFoundError:
    print('No local settings.')
    pass


# DJANGO BASE SETTINGS
# =========================
# Sécurité
SECRET_KEY = 'SECRET_KEY' in server_settings and server_settings['SECRET_KEY'] or 'olqk+0l-a0yh@!yd%+#6-0n5l8k8&_^ncmo_4$q==2l$+@knq0'
ALLOWED_HOSTS = 'ALLOWED_HOSTS' in server_settings and server_settings['ALLOWED_HOSTS'] or ['0.0.0.0', '127.0.0.1', 'localhost']
INTERNAL_IPS = ['127.0.0.1']

# Debug
DEBUG = 'DEBUG' in server_settings and server_settings['DEBUG'] or False
TEMPLATE_DEBUG = 'DEBUG' in server_settings and server_settings['DEBUG'] or False


# WSGI et routage
ROOT_URLCONF = 'configuration.urls'
WSGI_APPLICATION = 'configuration.wsgi.application'

# Traduction et Régionalisation (https://docs.djangoproject.com/en/1.6/topics/i18n/)
TIME_ZONE = 'Europe/Paris'
LANGUAGE_CODE = 'fr'  # bootstrap3_datetime nécessite un code en 2 lettres
USE_I18N = True
USE_L10N = True
USE_TZ = True
DATETIME_INPUT_FORMATS = ['%d/%m/%Y %H:%M', '%d/%m/%Y %H:%M:%s']
DATE_INPUT_FORMATS = ['%d/%m/%Y']
DATE_FORMAT = "d/m/Y"
DATETIME_FORMAT = "d F Y H:i"

# Framework Sites (https://docs.djangoproject.com/en/1.6/ref/contrib/sites/)
SITE_ID = 1

# Fichiers statiques et fichiers uploadés (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/
# https://docs.djangoproject.com/en/1.6/topics/files/
STATIC_URL = '/static/'
STATICFILES_DIRS = []
# Utiliser Paths.get_root_dir pour un répertoire relatif à celui du projet
STATIC_ROOT = 'STATIC_ROOT' in server_settings and server_settings['STATIC_ROOT'] or '/tmp/static/'
MEDIA_ROOT = 'MEDIA_ROOT' in server_settings and server_settings['MEDIA_ROOT'] or '/tmp/media/'
MEDIA_URL = '/media/'

# Limite mémoire d'upload de fichier dans Django (mais pas dans Apache/Nginx)
FILE_UPLOAD_MAX_MEMORY_SIZE = 10485760  # 10 Mégaoctets

# Email backend (https://docs.djangoproject.com/en/1.6/topics/email/#email-backends)
EMAIL_BACKEND = ('EMAIL_BACKEND' in server_settings and server_settings['EMAIL_BACKEND'] == 'smtp'
                 ) and 'django.core.mail.backends.smtp.EmailBackend' or 'django.core.mail.backends.console.EmailBackend'
EMAIL_HOST = 'EMAIL_HOST' in server_settings and server_settings['EMAIL_HOST'] or ''
EMAIL_PORT = 587
EMAIL_HOST_USER = 'EMAIL_HOST_USER' in server_settings and server_settings['EMAIL_HOST_USER'] or ''
EMAIL_HOST_PASSWORD = 'EMAIL_HOST_PASSWORD' in server_settings and server_settings['EMAIL_HOST_PASSWORD'] or ''
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = 'DEFAULT_FROM_EMAIL' in server_settings and server_settings['DEFAULT_FROM_EMAIL'] or 'ne-pas-repondre@ecomanif-sport-auvergne-rhone-alpes.fr'
ADMINS = [('Chris', 'christian.lopez@openscop.fr'), ('David', 'david.rechatin@openscop.fr')]

# DATABASE SETTINGS
# =================
# Base de données principale (https://docs.djangoproject.com/en/1.6/ref/settings/#databases)
DATABASES = OrderedDict([
    ['default', {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'DATABASE_NAME' in server_settings and server_settings['DATABASE_NAME'] or 'default',
        'USER': 'DATABASE_USER' in server_settings and server_settings['DATABASE_USER'] or 'user',
        'PASSWORD': 'DATABASE_PASSWORD' in server_settings and server_settings['DATABASE_PASSWORD'] or 'password',
        'HOST': 'localhost',
        'PORT': 5432,
        'NUMBER': 42
    }]
])

# CACHE SETTINGS
# ==============
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
    }
}

# DJANGO USER AND AUTH SETTINGS
# =============================
# Cookie
# SESSION_COOKIE_DOMAIN=".manifestationsportive.fr"

# Modèle utilisateur utilisé par Django (1.5+)
AUTH_USER_MODEL = 'core.user'

# Authentification (https://docs.djangoproject.com/en/1.6/ref/settings/#authentication-backends)
AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

# Password hashers (https://docs.djangoproject.com/en/1.6/ref/settings/#password-hashers)
PASSWORD_HASHERS = (
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'core.util.hashers.DrupalPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.UnsaltedMD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
)

# TEMPLATES & MIDDLEWARE
# ======================
# Configuration du moteur de templates
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            Paths.get_root_dir('portail', 'templates'),
            Paths.get_root_dir('templates'),
        ],
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.request',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
                'core.processors.core.core',
                'aide.processors.contexthelp.contexthelps'
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ],
            'debug': DEBUG,
        },
    },
]

# Middleware
MIDDLEWARE_MINI = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

MIDDLEWARE_DEV = (
    # 'debug_toolbar.middleware.DebugToolbarMiddleware',
)

MIDDLEWARE_PROD = ()

MIDDLEWARE = ('DEVELOPMENT_MODE' in server_settings and server_settings['DEVELOPMENT_MODE']
              ) and (MIDDLEWARE_MINI + MIDDLEWARE_DEV) or (MIDDLEWARE_MINI + MIDDLEWARE_PROD)


# APPLICATIONS
# ============
INSTALLED_APPS_MINI = (
    'django.contrib.admin',
    'django.contrib.admindocs',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.flatpages',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
    'django_extensions',
    'django_jenkins',
    'adminsortable2',
    'bootstrap_datepicker_plus',
    'ajax_select',
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.google',
    'captcha',
    'ckeditor',
    'ckeditor_uploader',
    'crispy_forms',
    'import_export',
    'fixture_magic',
    'administrative_division',
    'aide',
    'contacts',
    'core',
    'evenements',
    'notifications',
    'nouveautes',
    'organisateurs',
    'editeurs',
    'referents',
    'portail',
    'parcs_naturels',
    'sports',
    'clever_selects',
)
INSTALLED_APPS_DEV = (
    'django_custom_user_migration',
    # 'debug_toolbar',
)
INSTALLED_APPS_PROD = (
)

INSTALLED_APPS = ('DEVELOPMENT_MODE' in server_settings and server_settings['DEVELOPMENT_MODE']
                  ) and (INSTALLED_APPS_MINI + INSTALLED_APPS_DEV) or (INSTALLED_APPS_MINI + INSTALLED_APPS_PROD)

# LOGGING
# ==================================
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'smtp_file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': '/var/log/ecomanif/smtp.debug.log',
            'formatter': 'verbose'
        },
        'mail_admins': {
            'level': 'ERROR',
            'class': 'django.utils.log.AdminEmailHandler',
            'include_html': True,
            'email_backend': 'django.core.mail.backends.smtp.EmailBackend',
        },
    },
    'formatters': {
        'verbose': {'format': '%(levelname)s %(asctime)s %(module)s (pid %(process)d) %(message)s'},
        'simple': {'format': '%(levelname)s | %(asctime)s | %(message)s'},
    },
    'loggers': {
        'smtp': {
            'handlers': ['smtp_file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

# APPLICATION MANIFESTATION SPORTIVE
# ==================================
# URL principale du site.
MAIN_URL = 'MAIN_URL' in server_settings and server_settings['MAIN_URL'] or 'http://localhost:8003/'

# Paramètre de maintenance : désactiver la création auto d'avis etc.
DISABLE_SIGNALS = False

# Alias de formulaire pour la validation auto
FORM_ALIASES = {
    'manifestation': ['evenements.forms.manif.ManifForm'],
    'umanifestation': ['evenements.forms.sansinstance.SansInstanceManifForm']
}

# TESTS
# =====
TESTS_IN_PROGRESS = False
if 'test' in sys.argv[1:] or 'jenkins' in sys.argv[1:]:
    logging.disable(logging.CRITICAL)
    DEBUG = False
    TEMPLATE_DEBUG = False
    TESTS_IN_PROGRESS = True
    MIGRATION_MODULES = DisableMigrations()
    DATABASES = {k: v for k, v in DATABASES.items() if k is 'default'}
    DISABLE_SIGNALS = False

# FUSION des bases / LEGACY
# =========================
if 'full_merge' in sys.argv[1:] or 'import_external' in sys.argv[1:]:
    DISABLE_SIGNALS = True  # Ne surtout pas conserver le comportement des signaux lors de l'importation

# PROTECTION de base du code sensible
# ===================================
PROTECT_CODE = False  # Certaines commandes ne peuvent pas s'exécuter si == True

# SETTINGS RUNSERVER
# ==================
for arg in sys.argv[1:]:
    if 'runserver' in arg or 'unicorn' in arg:
        DISABLE_SIGNALS = False

# Empêcher l'effacement de .thirdparty lors du nettoyage automatique des imports
for key in thirdparty.__dict__:
    if key == key.upper():
        locals()[key] = getattr(thirdparty, key)

# CRISPY
# ======
CRISPY_TEMPLATE_PACK = 'bootstrap4'

# django-simple-captcha
# =====================
CAPTCHA_FOREGROUND_COLOR = '#013A50'
CAPTCHA_BACKGROUND_COLOR = '#BAC420'
CAPTCHA_FONT_SIZE = 28
CAPTCHA_LENGTH = 6

SESSION_EXPIRE_AT_BROWSER_CLOSE = True

# SENTRY
# ======
# Exceptions logging :
# https://sentry.io/Openscop/ecomanif/
SENTRY_CONFIG = 'SENTRY_CONFIG' in server_settings and server_settings['SENTRY_CONFIG'] or '',
if SENTRY_CONFIG:
    sentry_sdk.init(
        dsn=SENTRY_CONFIG[0],
        integrations=[DjangoIntegration()],
    )

# allauth settings
# ================
SOCIALACCOUNT_PROVIDERS = {
    'google': {
        'SCOPE': ['profile', 'email'],
        'AUTH_PARAMS': {'access_type': 'online'}
    },
}
