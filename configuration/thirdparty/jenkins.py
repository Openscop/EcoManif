# coding: utf-8

# JENKINS SETTINGS
# ==============================
# Configuration Jenkins
PEP8_RCFILE = 'configuration/pep8.rc'
PYLINT_LOAD_PLUGIN = ['pylint_django']

# List of applications tested when './manage.py jenkins' command (https://github.com/kmmbvnr/django-jenkins/)
PROJECT_APPS = (
    'administration',
    'administrative_division',
    'agreements',
    'authorizations',
    'carto',
    'contacts',
    'declarations',
    'emergencies',
    'evaluations',
    'events',
    'notifications',
    'nouveautes',
    'organisateurs',
    'parcs_naturelsparcs_naturels',
    'sports',
    'sub_agreements',
)

# List of Jenkins tasks executed by './manage.py jenkins' command (https://github.com/kmmbvnr/django-jenkins/)
JENKINS_TASKS = (
    'django_jenkins.tasks.run_pylint',
    'django_jenkins.tasks.run_pep8',
)
