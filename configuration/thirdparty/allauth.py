# coding: utf-8

# ALLAUTH SETTINGS
# ================
# Account Settings (https://github.com/pennersr/django-allauth)
# Supprimé pour fonctionner avec plusieurs signupForms
# ACCOUNT_SIGNUP_FORM_CLASS = 'core.forms.SignupForm'
ACCOUNT_EMAIL_VERIFICATION = 'mandatory'
ACCOUNT_EMAIL_CONFIRMATION_ANONYMOUS_REDIRECT_URL = '/accounts/email_confirmed'
ACCOUNT_ADAPTER = 'core.util.allauth.EcomanifAccountAdapter'
SOCIALACCOUNT_ADAPTER = 'core.util.allauth.EcomanifSocialAccountAdapter'
ACCOUNT_AUTHENTICATED_LOGIN_REDIRECTS = False
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
