# coding: utf-8
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.forms import models
from django.forms.fields import TypedMultipleChoiceField
from django.utils import timezone

from aide.models.helpaccordion import HelpAccordionPage, HelpAccordionPanel
from core.util.user import UserHelper


class HelpAccordionPageForm(models.ModelForm):
    """ Formulaire admin des pages d'aide avec accordion """
    model = HelpAccordionPage

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        if not 'path' in self.initial:
            self.initial['path'] = 'accordion/'
        if 'role' in self.initial:
            self.initial['role'] = self.initial['role'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'contentbefore': CKEditorUploadingWidget(), 'contentafter': CKEditorUploadingWidget()}


class HelpAccordionPanelForm(models.ModelForm):
    """ Formulaire admin des panneaux de page d'aide avec accordion """
    model = HelpAccordionPanel

    # Champs
    role = TypedMultipleChoiceField(choices=UserHelper.ROLE_CHOICES, required=False, label="Rôle")

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        #self.fields['role'].widget.attrs = {'data-placeholder': "Laissez vide pour tous"}
        self.fields['departements'].help_text = None
        if 'role' in self.initial and self.initial['role']:
            self.initial['role'] = self.initial['role'].split(',')

    def save(self, commit=True):
        """Sauvegarde de l'objet"""
        self.instance.updated = timezone.now()
        self.instance.role = ','.join(sorted(self.cleaned_data['role']))
        return super().save(commit=commit)

    class Meta:
        exclude = []
        widgets = {'content': CKEditorUploadingWidget()}
