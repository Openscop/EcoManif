/**
 * Created by knt on 16/05/16.
 */

// Cache des données récupérées en AJAX
var cachedData = Array();


function retrieveData() {
    // Charger les données de tooltip et les mettre en cache
    var element = $(this); // élément du DOM
    var name = $.grep(this.className.split(" "), function(v, i){
       return v.indexOf('ctx-help-') === 0;
   }).join(); // nom de l'élément

    // Utiliser les données de l'élément si elles sont en cache
    if (name in cachedData) {
        return cachedData[name];
    }

    var localData = "";
    $.ajax('/aide/context/' + name, {
        async: false,
        success: function (data) {
            localData = data;
            cachedData[name] = localData;
        }
    });

    // Renvoyer les données récupérées (ou pas)
    return localData;
}


$(function () {
    // Lors du clic sur un élément
    $('[class^=ctx-help-]').tooltip({
                                    title: retrieveData,
                                    html: true,
                                    container: 'body',
                                    trigger: 'click hover focus',
                                    placement: 'top',
                                });
    $('[class^=ctx-help-]').on('inserted.bs.tooltip', function () {
            AfficherIcones();
    })
});


