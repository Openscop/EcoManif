# coding: utf-8
from django.contrib import admin
from django.db.models import Q
from import_export.admin import ImportExportActionModelAdmin

from administrative_division.models.departement import Departement
from aide.forms import HelpPageForm, HelpNoteForm
from aide.models.help import HelpPage, HelpNote
from core.util.admin import RelationOnlyFieldListFilter


class HelpNoteInline(admin.StackedInline):
    """ Inline pour ajouter des notes aux pages d'aide """

    # Configuration
    model = HelpNote
    fields = (('title', 'active'), 'role', 'departements', 'content')
    extra = 1
    form = HelpNoteForm

    # Overrides
    def get_queryset(self, request):
        """ Modifier le queryset selon l'utilisateur """
        if request.user.is_superuser:
            # Les superutilisateurs peuvent tout voir
            return super().get_queryset(request)
        else:
            # Les autres voient ce qui correspond à leur département
            departement = request.user.get_instance().get_departement()
            return super().get_queryset(request).filter(Q(departements=departement) | Q(departements__isnull=True))

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if request.user.is_superuser or db_field.name != 'departements':
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            departement = request.user.get_instance().get_departement()
            field = super().formfield_for_foreignkey(db_field, request, **kwargs)
            field.queryset = Departement.objects.filter(id=departement.pk)
            return field


@admin.register(HelpPage)
class HelpPageAdmin(ImportExportActionModelAdmin):
    """ Administration des pages d'aide """

    # Configuration
    list_select_related = True
    list_display = ['pk', 'title', 'path', 'active', 'get_note_count', 'updated']
    list_display_links = []
    list_filter = ['active', ('departements', RelationOnlyFieldListFilter)]
    list_editable = ['active']
    readonly_fields = []
    search_fields = ['title', 'content', 'path', 'slug']
    actions = []
    exclude = []
    form = HelpPageForm
    inlines = [HelpNoteInline]
    actions_on_top = True
    order_by = ['pk']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        """ Modifier le queryset selon l'utilisateur """
        if request.user.is_superuser or request.user.get_instance().name == '(Master)':
            # Les superutilisateurs peuvent tout voir
            return super().get_queryset(request)
        else:
            # Les autres voient ce qui correspond à leur département
            departement = request.user.get_instance().get_departement()
            return super().get_queryset(request).filter(Q(departements=departement) | Q(departements__isnull=True))

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if request.user.is_superuser or request.user.get_instance().name == '(Master)' or db_field.name != 'departements':
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            departement = request.user.get_instance().get_departement()
            field = super().formfield_for_foreignkey(db_field, request, **kwargs)
            field.queryset = Departement.objects.filter(id=departement.pk)
            return field
