# coding: utf-8
from django.contrib import admin
from django.db.models import Q
from import_export.admin import ImportExportActionModelAdmin
from adminsortable2.admin import SortableAdminMixin, SortableInlineAdminMixin

from administrative_division.models.departement import Departement
from aide.forms import HelpAccordionPageForm, HelpAccordionPanelForm
from aide.models.helpaccordion import HelpAccordionPage, HelpAccordionTab, HelpAccordionPanel
from core.util.admin import RelationOnlyFieldListFilter


class HelpAccordionPanelInline(SortableInlineAdminMixin, admin.StackedInline):
    """ Inline pour ajouter des notes aux pages d'aide """

    # Configuration
    model = HelpAccordionPanel
    fields = (('title', 'active'), 'role', 'departements', 'updated', 'content')
    readonly_fields = ('updated',)
    extra = 1
    form = HelpAccordionPanelForm

    # Overrides
    def get_queryset(self, request):
        """ Modifier le queryset selon l'utilisateur """
        if request.user.is_superuser:
            # Les superutilisateurs peuvent tout voir
            return super().get_queryset(request)
        else:
            # Les autres voient ce qui correspond à leur département
            departement = request.user.get_instance().get_departement()
            return super().get_queryset(request).filter(Q(departements=departement) | Q(departements__isnull=True))

    def formfield_for_foreignkey(self, db_field, request=None, **kwargs):
        if request.user.is_superuser or db_field.name != 'departements':
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            departement = request.user.get_instance().get_departement()
            field = super().formfield_for_foreignkey(db_field, request, **kwargs)
            field.queryset = Departement.objects.filter(id=departement.pk)
            return field


class HelpAccordionTabInLine(SortableInlineAdminMixin, admin.StackedInline):
    model = HelpAccordionTab
    extra = 1
    show_change_link = True


@admin.register(HelpAccordionPage)
class HelpAccordionPageAdmin(ImportExportActionModelAdmin):
    """ Administration des pages d'aide avec accordion"""

    # Configuration
    list_select_related = True
    list_display = ['pk', 'title', 'path', 'active', 'get_tabs_count', 'get_panels_count', 'updated']
    list_display_links = []
    list_filter = ['active', ('departements', RelationOnlyFieldListFilter)]
    list_editable = ['active']
    readonly_fields = []
    search_fields = ['title', 'content', 'path', 'slug']
    actions = []
    exclude = []
    form = HelpAccordionPageForm
    inlines = [HelpAccordionTabInLine]
    actions_on_top = True
    order_by = ['pk']
    list_per_page = 50

    # Overrides
    def get_queryset(self, request):
        """ Modifier le queryset selon l'utilisateur """
        if request.user.is_superuser:
            # Les superutilisateurs peuvent tout voir
            return super().get_queryset(request)
        else:
            # Les autres voient ce qui correspond à leur département
            departement = request.user.get_instance().get_departement()
            return super().get_queryset(request).filter(Q(departements=departement) | Q(departements__isnull=True))

    def formfield_for_manytomany(self, db_field, request=None, **kwargs):
        if request.user.is_superuser or db_field.name != 'departements':
            return super().formfield_for_foreignkey(db_field, request, **kwargs)
        else:
            departement = request.user.get_instance().get_departement()
            field = super().formfield_for_foreignkey(db_field, request, **kwargs)
            field.queryset = Departement.objects.filter(id=departement.pk)
            return field


@admin.register(HelpAccordionTab)
class HelpAccordionTabAdmin(SortableAdminMixin, ImportExportActionModelAdmin):
    """ Administration des pages d'aide avec accordion"""

    # Configuration
    inlines = [HelpAccordionPanelInline]
    list_display = ('title', 'page')

    def has_module_permission(self, request):
        return False


@admin.register(HelpAccordionPanel)
class HelpAccordionPanelAdmin(SortableAdminMixin, ImportExportActionModelAdmin):
    """ Administration des pages d'aide avec accordion"""

    # Configuration
    list_display = ('title', 'tab')

    def has_module_permission(self, request):
        return False
