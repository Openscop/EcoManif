# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from aide.forms import ContextHelpForm
from aide.models.context import ContextHelp


@admin.register(ContextHelp)
class ContextHelpAdmin(ImportExportActionModelAdmin):
    """ Administration des aides contextuelles """

    # Configuration
    list_select_related = True
    list_display = ['pk', 'name', 'active']
    list_display_links = []
    list_filter = ['active']
    list_editable = ['active']
    readonly_fields = []
    search_fields = ['name']
    actions = []
    exclude = []
    actions_on_top = True
    order_by = ['pk']
    form = ContextHelpForm
    list_per_page = 50
