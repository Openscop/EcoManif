# coding: utf-8
"""
Préprocesseurs de requête
"""

from aide.models.context import ContextHelp

def contexthelps(request):
    contexthelps = []
    if hasattr(request.resolver_match, 'url_name'):
        contexthelps = [ contexthelp for contexthelp in ContextHelp.objects.filter(page_names__contains=request.resolver_match.url_name) if contexthelp.is_visible(request)  ]
    return {'contexthelps': contexthelps}