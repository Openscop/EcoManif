# coding: utf-8
from django.test import TestCase

from aide.models.context import ContextHelp
from organisateurs.factories import OrganisateurFactory
from core.factories import UserFactory

class ContextTests(TestCase):
    """ Tester les fonctionnalités des aides contextuelles """

    def test_identifier_detection(self):
        """
        Test des identifiants

        Teste qu'un identifiant (ex ctx-help-foo ctx-help-bar)
        renvoie bien les aides contextuelles avec les noms foo
        et bar.
        """
        ctx1 = ContextHelp.objects.create(name='foo', text="Foo")
        ctx2 = ContextHelp.objects.create(name='bar', text="Bar")
        self.assertEquals(ContextHelp.objects.for_identifier('ctx-help-foo ctx-help-bar').count(), 2)
        self.assertEquals(ContextHelp.objects.for_identifier('ctx-help-foo').count(), 1)
        self.assertEquals(ContextHelp.objects.for_identifier('ctx-help-foo-bar').count(), 0)
        self.assertTrue(ctx1 in ContextHelp.objects.for_identifier('ctx-help-foo'))
        self.assertTrue(ctx2 in ContextHelp.objects.for_identifier('ctx-help-bar'))

    def test_role_detection(self):
        """
        Test des rôles

        Vérifier que les rôles auxquels est assignée une aide contextuelle
        peuvent voir cette aide, et seulement eux.
        """
        ctx1 = ContextHelp.objects.create(name='foo', text="Foo", role="organisateur")
        agent = OrganisateurFactory.build(user__username='agent1')
        user = UserFactory.build(username='user1')
        # Normalement, un agent de mairie peut voir l'aide, mais pas l'instructeur
        self.assertTrue(ctx1.is_visible(agent.user))
        self.assertFalse(ctx1.is_visible(user))
