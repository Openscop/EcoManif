from django.db import models
from django.conf import settings


class Demande(models.Model):
    """ Modèle de demande pour le formulaire de contact """
    CHOIX_TYPE = [(0, "Demande d'assistance à l'inscription"),
                  (1, "Demande d'assistance à la déclaration de manifestation"),
                  (2, "Demande d'assistance pour la charte"),
                  (3, "Autre demande")]

    CHOIX_STATUS = [(0, "Nouvelle demande"),
                    (1, "En cours de traitement"),
                    (2, "Traitée"),
                    (3, "Non pertinent")]

    type = models.SmallIntegerField("Objet de la demande", choices=CHOIX_TYPE, default=2)
    departement = models.ForeignKey('administrative_division.departement', verbose_name="Département concerné", blank=False, on_delete=models.CASCADE)
    contenu = models.TextField("Exposez votre demande", blank=False)
    email = models.EmailField("adresse de courriel")
    user = models.ForeignKey(settings.AUTH_USER_MODEL, related_name="demandeur", verbose_name="utilisateur", blank=True, null=True, on_delete=models.SET_NULL)
    status = models.SmallIntegerField("Etat de la demande", choices=CHOIX_STATUS, default=0)
    date = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Formulaire de contact'
        verbose_name_plural = 'Formulaires de contact'
