# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin

from nouveautes.forms import NouveauteForm
from nouveautes.models.nouveaute import Nouveaute


@admin.register(Nouveaute)
class NouveauteAdmin(ImportExportActionModelAdmin):
    """ Configuration admin des nouveautés """

    list_display = ['pk', 'title', 'creation', 'public', 'get_departements', 'get_role_names_shorter']
    list_filter = ['creation', 'public', 'departements']
    search_fields = ['title', 'body']
    form = NouveauteForm
    list_per_page = 25
