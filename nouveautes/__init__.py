# coding: utf-8
"""
Cette application gère l'affichage des nouveautés à certains rôles.
Les nouveautés sont affichées aux membres concernés jusqu'à ce qu'ils
en prennent connaissance.
"""
from django.apps.config import AppConfig


class NouveautesConfig(AppConfig):
    """ Configuration de l'application nouveautés """

    name = 'nouveautes'
    verbose_name = 'Nouveautés de la plateforme'


default_app_config = 'nouveautes.NouveautesConfig'
