# coding: utf-8
from django.test import TestCase

from evenements.factories import AvtmFactory

class OrganisateurMethodTests(TestCase):
    """ Tests des organisateurs """

    def setUp(self):
        """ Configuration """
        """
        Tests : Créer une manifestation ainsi que la demande d'autorisation

        Créer les services administratifs nécessaires lors de la création d'une
        manifestation sportive. La manifestation créée ici est une manifestation
        sportive non motorisée soumise à autorisation.
        """
        self.manifestation = AvtmFactory.create()
        self.commune = self.manifestation.ville_depart
        self.departement = self.commune.get_departement()
        self.arrondissement = self.commune.get_arrondissement()
        self.instance = self.departement.get_instance()
        self.activite = self.manifestation.activite
        self.structure = self.manifestation.structure
        self.organisateur = self.structure.organisateur

    def test_properties(self):
        """ Vérifier que les propriétés/méthodes renvoient les valeurs attendues """
        self.assertEqual(self.organisateur.get_departement(), self.structure.commune.arrondissement.departement)
