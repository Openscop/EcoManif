# coding: utf-8
from django.contrib import admin
from django.core.exceptions import ObjectDoesNotExist
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from core.util.admin import set_admin_info, RelationOnlyFieldListFilter
from organisateurs.models import Organisateur
from core.models.user import User

@admin.register(Organisateur)
class OrganisateurAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Admin des organisateurs """

    # Configuration
    list_display = ['pk', 'user', 'structure__name', 'structure__commune__arrondissement__departement']
    list_filter = ['cgu', ('structure__commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['user__username', 'user__first_name', 'user__last_name', 'user__email']
    inlines = []

    # Overrides
    def lookup_allowed(self, lookup, value):
        if lookup.startswith('structure__commune__arrondissement__departement'):
            return True
        return super().lookup_allowed(lookup, value)

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser or request.user.get_instance().name == '(Master)':
            return queryset
        return queryset.filter(structure__commune__arrondissement__departement=request.user.get_departement())

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['user'].queryset = User.objects.filter(default_instance=request.user.get_instance())
        return form
