# coding: utf-8
from django.contrib import admin
from import_export.admin import ImportExportActionModelAdmin
from related_admin import RelatedFieldAdmin

from core.util.admin import RelationOnlyFieldListFilter
from organisateurs.forms.structure import StructureForm
from organisateurs.models import Structure, StructureType, Organisateur


@admin.register(StructureType)
class StructureTypeAdmin(ImportExportActionModelAdmin):
    pass


@admin.register(Structure)
class StructureAdmin(ImportExportActionModelAdmin, RelatedFieldAdmin):
    """ Administration des structures organisatrices """

    list_display = ['pk', 'name', 'type_of_structure', 'organisateur', 'commune__arrondissement__departement']
    list_filter = ['type_of_structure__type_of_structure', ('commune__arrondissement__departement', RelationOnlyFieldListFilter)]
    search_fields = ['name']
    form = StructureForm

    # Overrides
    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if request.user.is_superuser or request.user.get_instance().name == '(Master)':
            return queryset
        return queryset.filter(commune__arrondissement__departement=request.user.get_departement())

    def get_form(self, request, obj=None, **kwargs):
        form = super().get_form(request, obj=None, **kwargs)
        if request.user.get_departement():
            form.base_fields['organisateur'].queryset = Organisateur.objects.filter(user__default_instance=request.user.get_instance())
        return form
