# coding: utf-8
from django.contrib.contenttypes.fields import GenericRelation
from django.db import models

from administrative_division.mixins.divisions import ManyPerDepartementQuerySetMixin, DepartementableMixin


class ParcNaturelQuerySet(ManyPerDepartementQuerySetMixin):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, name):
        return self.get(name=name)


class ParcNaturel(models.Model):
    """ Parc Naturel Régional et national """

    # Champs
    name = models.CharField("nom", max_length=255, unique=True)
    departements = models.ManyToManyField(to='administrative_division.Departement', verbose_name='départements concernés')
    code = models.PositiveIntegerField("code", unique=True)
    objects = ParcNaturelQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.name

    def natural_key(self):
        """ Clé naturelle """
        return self.name,

    # Meta
    class Meta:
        verbose_name = "parc naturel national ou régional"
        verbose_name_plural = "parcs naturels nationals et régionals"
        default_related_name = "parcsnaturels"
        app_label = "parcs_naturels"


class AdministrateurParcNaturelQuerySet(ManyPerDepartementQuerySetMixin):
    """ Queryset """

    # Overrides
    def get_by_natural_key(self, parcnat, conservator, name):
        return self.get(pn=parcnat, conservator=conservator, name=name)


class AdministrateurParcNaturel(DepartementableMixin):
    """ Administrateur RNR """

    # Champs
    short_name = models.CharField("abréviation", max_length=10)
    name = models.CharField("nom", max_length=255)
    address = models.CharField("adresse", max_length=255)
    commune = models.ForeignKey('administrative_division.commune', verbose_name="commune", on_delete=models.CASCADE)
    email = models.EmailField("e-mail", max_length=200)
    conservator = GenericRelation("contacts.contact", verbose_name="conservateur")
    parcnat = models.OneToOneField("parcs_naturels.ParcNaturel", related_name="administrateursparcnaturel", verbose_name="parc naturel", on_delete=models.CASCADE)
    objects = AdministrateurParcNaturelQuerySet.as_manager()

    # Override
    def __str__(self):
        """ Renvoyer la représentation de l'objet """
        return self.short_name

    def natural_key(self):
        """ Clé naturelle """
        return self.parcnat, self.conservator, self.name

    # Meta
    class Meta:
        verbose_name = "Gestionnaire Parc Naturel"
        verbose_name_plural = "Gestionnaires Parc Naturel"
        default_related_name = "administrateursparcnaturel"
        app_label = "parcs_naturels"
