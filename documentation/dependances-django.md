Dépendances Python et Django
===

### beautifulsoup4
https://beautiful-soup-4.readthedocs.io/en/latest/

Librairie qui permet de parser une page html et ainsi de pourvoir accéder à n'importe quel élément du DOM sous la forme d'un dictionnaire.

Utilisée dans les apps aide et nouveauté

### Django
https://docs.djangoproject.com

On ne le présente plus... Tutoriel disponible

### django-admin-sortable2
https://django-admin-sortable2.readthedocs.io/en/latest/

Fournit des classes mixin qui permettent, dans l'admin, de pourvoir changer l'ordre des inlines ou des items dans les listes.

Utilisée dans l'appli "aide" pour maitriser l'ordre des onglets et des panneaux dans les pages de type "accordion".

### django-ajax-selects
https://django-ajax-selects.readthedocs.io/en/latest/

Permet d'éditer des champs ForeignKey, ManyToManyField et CharField en utilisant jQuery UI AutoComplete.<br>
Il faut définir un fichier lookups pour enregistrer un canal, ensuite, grâce à la méthode "make_ajax_form", un formulaire est créé avec les champs où la librairie ajoute l'autocomplètion.

Utilisée essentiellement dans l'admin des applis "events", "evenements", "instructions" et "authorizations"

### django-allauth
https://github.com/pennersr/django-allauth

Application django gérant les processus d'authentification (validation de l'adresse email, 	changement d'adresse email, etc...)

### django-annoying
https://github.com/skorokithakis/django-annoying/blob/master/README.md

Librairie qui fournit des fonctionnalités qui réduisent les lignes de code ou reprennent certaines fonctions pour ajouter un plus.

Utilisée dans les applis "aide" (render_to) et "nouveauté" (AutoOneToOneField).

### django-bootstrap-datepicker-plus
https://github.com/monim67/django-bootstrap-datepicker-plus

Fournit un widget basé sur bootstrap pour les champs DateTimeField.

Utilisée dans les formulaires où un champ date est requis et dans les filtres de l'API.

### django-ckeditor
https://github.com/Openscop/django-ckeditor

Application permettant d'intégrer l'éditeur wysiwyg ckeditor

### django-clever-selects
https://github.com/PragmaticMates/django-clever-selects

Application permettant de chainer des SelecBox entre elles. Typiquement, chaine la sélection d'un département avec la sélection d'un service consulté.

Utilisation généralisée dans tout le projet.

### django-crispy-forms
https://github.com/maraujop/django-crispy-forms/

Application permettant de générer plus facilement des formulaires avec les thèmes Bootstrap

### django-extensions
https://github.com/django-extensions/django-extensions

Divers utilitaires pour aider au développement d'applis django :
    • python manage.py runserver_plus 
    • python manage.py shell_plus  --- Shell interactif avec import automatique des modèles de données  

### django-extra-views
https://github.com/AndrewIngram/django-extra-views

Permet de mixer des vues Update et Create avec des formulaires Inlines

### django-fixture-magic
https://github.com/davedash/django-fixture-magic

Librairie qui ajoute 4 nouvelles commandes à manage.py.
Ce sont : "dump_object", "merge_fixtrures", "reorder_fixtures" et "custom_dump".

### django-fsm
https://github.com/kmmbvnr/django-fsm

Application Django permettant de mettre simplement en place une machine à état sur un modèle.

### django-jenkins
https://github.com/kmmbvnr/django-jenkins/

Application permettant de lancer les tests unitaires ainsi que divers outils d'analyse du code :
- pep8 : conventions d'écriture python 
- pylint : idem un peu plus haut niveau 
- coverage : analyse de la couverture du code par les tests unitaires. Essayer de toujours rester au dessus de 80% de couverture

### django-model-utils
https://github.com/carljm/django-model-utils

Permet d'utiliser InheritanceManager qui facilite les queryset portant sur des classes héritées

### django-related-admin
https://github.com/PetrDlouhy/django-related-admin

Permet, au niveau de l'administration, d'introduire dans les list-display des relations traversantes sur des clés étrangères avec le double underscore.

Utilisation généralisée dans admin

### django-simple-captcha
https://django-simple-captcha.readthedocs.io/en/latest/index.html

Permet d'ajouter un champ captcha dans un formulaire.

Utilisation dans le formulaire de contact de l'appli "aide"

### factory_boy
https://github.com/rbarrois/factory_boy

Application pour une mise en oeuvre simple des fixtures pour les tests unitaires

### markdown
https://python-markdown.github.io/

Librairie markdown pour python.

### psycopg2
https://pypi.python.org/pypi/psycopg2

Pilote postgreSQL pour python

### python-memcached
https://pypi.org/project/python-memcached/

Gére la mise en cache.

### raven
https://github.com/getsentry/raven-python

Logge les exceptions générées en prod à travers la plateforme https://app.getsentry.com/

### selenium
https://www.seleniumhq.org/docs/03_webdriver.jsp

Toolkit pour la création de tests sur navigateur.

### simplejson
https://simplejson.readthedocs.io/en/latest/

Permet la manipulation d'objet Json.

Utilisée dans l'appli "core".

### unidecode
https://pypi.org/project/Unidecode/

Fournit une représentation ASCII d'un caractère codé en unicode.
