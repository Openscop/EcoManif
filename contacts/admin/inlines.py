# coding: utf-8
from django.contrib.contenttypes.admin import GenericTabularInline

from contacts.forms.contact import ContactForm
from ..models import Contact, Adresse


class ContactInline(GenericTabularInline):
    """ Inline admin des contacts """

    # Configuration
    model = Contact
    form = ContactForm
    extra = 0


class AddressInline(GenericTabularInline):
    """ Inline admin des adresses """

    # Confoguration
    model = Adresse
    extra = 0
    max_num = 1
