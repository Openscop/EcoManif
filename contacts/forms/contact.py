# coding: utf-8
from django import forms
from extra_views.generic import GenericInlineFormSet
# TODO : Renamed GenericInlineFormSet to GenericInlineFormSetFactory : django-extra-views 0.12.0 for future release. https://github.com/AndrewIngram/django-extra-views/blob/master/CHANGELOG.rst

from core.forms.base import GenericForm
from core.util.champ import TelField
from ..models import Contact


class ContactForm(GenericForm):
    """ Formulaire """

    # Champs
    phone = TelField()

    # Overrides
    def __init__(self, *args, **kwargs):
        super(ContactForm, self).__init__(*args, **kwargs)
        self.fields['phone'] = TelField()
        self.helper.form_tag = False

    # Meta
    class Meta:
        model = Contact
        exclude = ('content_type', 'object_id', 'content_object')


class ContactInline(GenericInlineFormSet):
    """ Formset Inline des conteacts """

    # Configuration
    model = Contact
    fields = ['first_name', 'last_name', 'phone']
    factory_kwargs = {'extra': 1, 'max_num': 1, 'can_delete': False}
    form_class = ContactForm
